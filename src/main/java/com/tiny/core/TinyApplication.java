package com.tiny.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 1. @SpringBootApplication  Spring Boot 应用的标识
 * 2. @MapperScan  			  mapper 接口类扫描包配置
 * 3. @ComponentScan		  组件扫描
 * 4. @EnableCaching          项目开启缓存  redis
 * 5. @EnableTransactionManagement  开事务
 */
@SpringBootApplication
@MapperScan("com.tiny.core.dao")
@ComponentScan(basePackages = {"com.tiny.core.*"})
@EnableCaching
@EnableTransactionManagement
public class TinyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TinyApplication.class, args);
	}

}
