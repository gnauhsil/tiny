package com.tiny.core.configuration;


import com.tiny.core.interceptor.LoggerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * Springboot2.0  使用 WebMvcConfigurer
 * WebMvcConfigurationSupport 在 2.0 过时
 */
@Configuration
public class LoginfoConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new LoggerInterceptor()).addPathPatterns("/**");
    }

}

