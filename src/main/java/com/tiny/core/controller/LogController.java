package com.tiny.core.controller;

import com.alibaba.fastjson.JSONObject;
import com.tiny.core.common.LoggerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by gnauhsil on 2018/6/5.
 */

/**
 * 1. LockBack 默认 INFO、ERROR、DEBUG 级别均输出
 * 2. resource 目录下 logback.xml 配置相关参数
 *
 */
@RestController
public class LogController {

    private final static Logger logger = LoggerFactory.getLogger(LogController.class);

    @RequestMapping(value = "/logTest")
    public String index(){
        logger.debug("记录debug日志");
        logger.info("访问了index方法");
        logger.error("记录error错误日志");
        return "index";
    }

    @RequestMapping(value = "/logLogin",method = RequestMethod.GET)
    public JSONObject login(HttpServletRequest request, String name) throws Exception{

        JSONObject obj = new JSONObject();
        obj.put("msg","用户："+name+"，登录成功。");
        request.setAttribute(LoggerUtils.LOGGER_RETURN,obj);
        return obj;
    }

}
