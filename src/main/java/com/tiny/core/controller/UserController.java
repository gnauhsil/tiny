package com.tiny.core.controller;

import com.alibaba.fastjson.JSONObject;
import com.tiny.core.common.LoggerUtils;
import com.tiny.core.domain.User;
import com.tiny.core.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by gnauhsil on 2018/5/29.
 */

/**
 * @RestController: 相当于 @Controller、@ResponseBody结合，返回json数据
 * @RestController：SpringMVC 视图解析器无法解析动、静态资源，不能返回jsp,html等页面，
 */
@Controller
@RequestMapping("/api/user")
public class UserController {

    private final static Logger logger = LoggerFactory.getLogger(UserController.class);
    /**
     * 实例化 UserService 类， 注入
     */
    @Autowired
    private UserService userService;

    /**
     * 访问方式：http://localhost:8080/singin
     * 视图解析器返回 singin 页面
     * @return
     */
    @RequestMapping("singin")
    public String singin(){
        return "singin";
    }

    @RequestMapping(value = "index",method={RequestMethod.POST,RequestMethod.GET})
    public String index(){
        return "login";
    }


    /**
     * value 定义访问路径
     * RequestMethod.GET： 定义访问方式为 get
     * @ResponseBody：定义返回类型为  json
     * @return
     */
    @RequestMapping(value = "list")
    public @ResponseBody List<User> findAllCity() {
        return userService.userList();
    }

    @RequestMapping(value = "add",method = RequestMethod.GET)
    public @ResponseBody String add(HttpServletRequest request, String userName){
        User user = new User();
        user.setUserName(userName);
        userService.add(user);
        return "redirect:" + "index";
    }






    /**
     * 返回类型为 String  使用 redirect 作转发
     * toUrl: 返回请求路径
     */

    @RequestMapping(value = "formLogin",method = RequestMethod.POST) //produces="application/json;charset=UTF-8"
    public String formLogin(HttpServletRequest request,User user) throws Exception {
        HttpSession session = request.getSession();
        String toUrl = "";
        User userVo = userService.findUserByName(user.getUserName());

        ModelAndView modelAndView = new ModelAndView();
        if(userVo != null){
            session.setAttribute("userVo",userVo);
            System.out.println("userVo   ===  " + userVo.getUserName());
            modelAndView.addObject("userVo", userVo);
            toUrl = "/week/report/index";
        }else{
            userVo = new User();
            userVo.setUserName("此用户不存在");
            System.out.println("此用户不存在");
            toUrl = "index";
        }

        return "redirect:" + toUrl;

    }

    /**
     * 入参： User user, key-value 格式，前端传入 json 报错
     * @ResponseBody 返回 User 对象，界面展现 json 数据格式
     */
   /* @RequestMapping(value = "login",method = RequestMethod.POST)
    public @ResponseBody User login(User user) throws Exception {

        System.out.println("userName = " + user.getUserName());

        User userVo = userService.findUserByName(user.getUserName());

        ModelAndView modelAndView = new ModelAndView();
        if(userVo != null){
            System.out.println("userVo   ===  " + userVo.getUserName());
            modelAndView.addObject("userVo", userVo);
            modelAndView.setViewName("index");
            System.out.println(RequestMethod.class.getName() + "   RequestMethod.class.getName()");
        }else{
            userVo = new User();
            userVo.setUserName("此用户不存在");
            modelAndView.setViewName("singin");
            System.out.println("此用户不存在");
        }

        return userVo;

    }*/


    /**
     * @RequestBody：1. 参数绑定，SpringMVC 将请求内容自动封装到 User 对象中
     *               2. 要求前端传入 json 字符串
     * ModelAndView：1.装载数据，回传到前端
     * 测试用  提交  json  返回 json
     */
    /*
    @RequestMapping(value = "login",method = RequestMethod.POST)
    public @ResponseBody User login(@RequestBody User user,HttpServletRequest request) throws Exception {

        HttpSession session = request.getSession();

        User userVo = userService.findUserByName(user.getUserName());

        ModelAndView modelAndView = new ModelAndView();
        if(userVo != null){
            session.setAttribute("userVo",userVo);
            System.out.println("userVo   ===  " + userVo.getUserName());
        }else{
            userVo = new User();
            userVo.setUserName("此用户不存在");
            System.out.println("此用户不存在");
        }

        return userVo;
    }*/

    @RequestMapping(value = "ajaxLogin",method = RequestMethod.POST)
    public @ResponseBody User ajaxLogin(HttpServletRequest request, @RequestBody User user) throws Exception {
        HttpSession session = request.getSession();

        User userVo = userService.findUserByName(user.getUserName());

        ModelAndView modelAndView = new ModelAndView();
        if(userVo != null){
            session.setAttribute("userVo",userVo);
            System.out.println("userVo   ===  " + userVo.getUserName());
        }else{
            userVo = new User();
            userVo.setUserName("no");
        }

        return userVo;

    }


    @RequestMapping(value = "userRedis",method = RequestMethod.GET)
    public JSONObject userRedis(HttpServletRequest request, String userName) {
        userService.userRedis(userName);

        JSONObject obj = new JSONObject();
        obj.put("msg","用户："+userName+"，查询成功。");
        request.setAttribute(LoggerUtils.LOGGER_RETURN,obj);

        return obj;
    }



}
