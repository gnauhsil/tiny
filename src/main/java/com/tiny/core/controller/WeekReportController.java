package com.tiny.core.controller;

import com.tiny.core.domain.User;
import com.tiny.core.domain.WeekReport;
import com.tiny.core.service.WeekReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by gnauhsil on 2018/5/29.
 */
@Controller
@RequestMapping(value="week")
public class WeekReportController {

    @Autowired
    public WeekReportService weekReportService;

    @GetMapping("report/indext")
    public String indext(){
        return "indext"; //当浏览器输入/index时，会返回 /static/home.html的页面
    }

    @GetMapping("report/index")
    public String index(){
        return "index"; //当浏览器输入/index时，会返回 /static/home.html的页面
    }

    /**
     * @RequestBody：1. 参数绑定，SpringMVC 将请求内容自动封装到 User 对象中
     *               2. 要求前端传入 json 字符串
     * ModelAndView：1.装载数据，回传到前端
     */
    @RequestMapping(value = "single/add",method = RequestMethod.POST)
    public void add(@RequestBody WeekReport weekReport) throws Exception {

        weekReportService.add(weekReport);

    }

    @GetMapping("report/pack")
    public void pack(){
        weekReportService.pack();
    }


}
