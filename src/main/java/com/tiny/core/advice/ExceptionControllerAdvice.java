package com.tiny.core.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by gnauhsil on 2018/9/8
 */

@ControllerAdvice
@ResponseBody
public class ExceptionControllerAdvice {
}
