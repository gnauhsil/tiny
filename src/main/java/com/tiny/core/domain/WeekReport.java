package com.tiny.core.domain;

/**
 * Created by connie_gnauhsil on 2018/6/5.
 */
public class WeekReport {
    private int id;
    private String weekDateStart;
    private String weekDateEnd;

    private String userSeq;

    private String currentNewCorecontent;
    private String currentTwoSecond;
    private String currentBasicManage;
    private String currentProjectUpdate;
    private String currentDemand;
    private String currentBid;
    private String currentBusiness;
    private String currentProjectProgess;
    private String currentTeamProcess;
    private String currentDaily;
    private String currentProblem;
    private String currentOther;


    private String nextNewCorecontent;
    private String nextTwoSecond;
    private String nextBasicManage;
    private String nextProjectUpdate;
    private String nextDemand;
    private String nextBid;
    private String nextBusiness;
    private String nextProjectProgess;
    private String nextTeamProcess;
    private String nextDaily;
    private String nextProblem;
    private String nextOther;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWeekDateStart() {
        return weekDateStart;
    }

    public void setWeekDateStart(String weekDateStart) {
        this.weekDateStart = weekDateStart;
    }

    public String getWeekDateEnd() {
        return weekDateEnd;
    }

    public void setWeekDateEnd(String weekDateEnd) {
        this.weekDateEnd = weekDateEnd;
    }

    public String getUserSeq() {
        return userSeq;
    }

    public void setUserSeq(String userSeq) {
        this.userSeq = userSeq;
    }

    public String getCurrentNewCorecontent() {
        return currentNewCorecontent;
    }

    public void setCurrentNewCorecontent(String currentNewCorecontent) {
        this.currentNewCorecontent = currentNewCorecontent;
    }

    public String getCurrentTwoSecond() {
        return currentTwoSecond;
    }

    public void setCurrentTwoSecond(String currentTwoSecond) {
        this.currentTwoSecond = currentTwoSecond;
    }

    public String getCurrentBasicManage() {
        return currentBasicManage;
    }

    public void setCurrentBasicManage(String currentBasicManage) {
        this.currentBasicManage = currentBasicManage;
    }

    public String getCurrentProjectUpdate() {
        return currentProjectUpdate;
    }

    public void setCurrentProjectUpdate(String currentProjectUpdate) {
        this.currentProjectUpdate = currentProjectUpdate;
    }

    public String getCurrentDemand() {
        return currentDemand;
    }

    public void setCurrentDemand(String currentDemand) {
        this.currentDemand = currentDemand;
    }

    public String getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(String currentBid) {
        this.currentBid = currentBid;
    }

    public String getCurrentBusiness() {
        return currentBusiness;
    }

    public void setCurrentBusiness(String currentBusiness) {
        this.currentBusiness = currentBusiness;
    }

    public String getCurrentProjectProgess() {
        return currentProjectProgess;
    }

    public void setCurrentProjectProgess(String currentProjectProgess) {
        this.currentProjectProgess = currentProjectProgess;
    }

    public String getCurrentTeamProcess() {
        return currentTeamProcess;
    }

    public void setCurrentTeamProcess(String currentTeamProcess) {
        this.currentTeamProcess = currentTeamProcess;
    }

    public String getCurrentDaily() {
        return currentDaily;
    }

    public void setCurrentDaily(String currentDaily) {
        this.currentDaily = currentDaily;
    }

    public String getCurrentProblem() {
        return currentProblem;
    }

    public void setCurrentProblem(String currentProblem) {
        this.currentProblem = currentProblem;
    }

    public String getCurrentOther() {
        return currentOther;
    }

    public void setCurrentOther(String currentOther) {
        this.currentOther = currentOther;
    }

    public String getNextNewCorecontent() {
        return nextNewCorecontent;
    }

    public void setNextNewCorecontent(String nextNewCorecontent) {
        this.nextNewCorecontent = nextNewCorecontent;
    }

    public String getNextTwoSecond() {
        return nextTwoSecond;
    }

    public void setNextTwoSecond(String nextTwoSecond) {
        this.nextTwoSecond = nextTwoSecond;
    }

    public String getNextBasicManage() {
        return nextBasicManage;
    }

    public void setNextBasicManage(String nextBasicManage) {
        this.nextBasicManage = nextBasicManage;
    }

    public String getNextProjectUpdate() {
        return nextProjectUpdate;
    }

    public void setNextProjectUpdate(String nextProjectUpdate) {
        this.nextProjectUpdate = nextProjectUpdate;
    }

    public String getNextDemand() {
        return nextDemand;
    }

    public void setNextDemand(String nextDemand) {
        this.nextDemand = nextDemand;
    }

    public String getNextBid() {
        return nextBid;
    }

    public void setNextBid(String nextBid) {
        this.nextBid = nextBid;
    }

    public String getNextBusiness() {
        return nextBusiness;
    }

    public void setNextBusiness(String nextBusiness) {
        this.nextBusiness = nextBusiness;
    }

    public String getNextProjectProgess() {
        return nextProjectProgess;
    }

    public void setNextProjectProgess(String nextProjectProgess) {
        this.nextProjectProgess = nextProjectProgess;
    }

    public String getNextTeamProcess() {
        return nextTeamProcess;
    }

    public void setNextTeamProcess(String nextTeamProcess) {
        this.nextTeamProcess = nextTeamProcess;
    }

    public String getNextDaily() {
        return nextDaily;
    }

    public void setNextDaily(String nextDaily) {
        this.nextDaily = nextDaily;
    }

    public String getNextProblem() {
        return nextProblem;
    }

    public void setNextProblem(String nextProblem) {
        this.nextProblem = nextProblem;
    }

    public String getNextOther() {
        return nextOther;
    }

    public void setNextOther(String nextOther) {
        this.nextOther = nextOther;
    }


}

