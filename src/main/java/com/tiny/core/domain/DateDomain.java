package com.tiny.core.domain;

/**
 * Created by connie_gnauhsil on 2018/6/20.
 */
public class DateDomain {

    private String weekDateStart;
    private String weekDateEnd;

    public String getWeekDateStart() {
        return weekDateStart;
    }

    public void setWeekDateStart(String weekDateStart) {
        this.weekDateStart = weekDateStart;
    }

    public String getWeekDateEnd() {
        return weekDateEnd;
    }

    public void setWeekDateEnd(String weekDateEnd) {
        this.weekDateEnd = weekDateEnd;
    }
}
