package com.tiny.core.domain;

import java.io.Serializable;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */

public class User implements Serializable {

    private int id;
    private String sysGroup;
    private String userName;
    private String passwd;


    public int getId() {
        return id;
    }

    public String getSysGroup() {
        return sysGroup;
    }

    public String getUserName() {
        return userName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSysGroup(String sysGroup) {
        this.sysGroup = sysGroup;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
}
