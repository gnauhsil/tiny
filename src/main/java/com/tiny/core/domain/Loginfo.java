package com.tiny.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

public class Loginfo implements Serializable{

    private String requestId;

    private String requestClientIp;

    private String requestUri;

    private String requestType;

    private String requestMethod;

    private String requestParamData;

    private String requestSessionId;

    private Timestamp requestTime;

    private String requestReturnTime;

    private String requestReturnData;

    private String requestHttpStatusCode;

    private int requestTimeConsuming;


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestClientIp() {
        return requestClientIp;
    }

    public void setRequestClientIp(String requestClientIp) {
        this.requestClientIp = requestClientIp;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestParamData() {
        return requestParamData;
    }

    public void setRequestParamData(String requestParamData) {
        this.requestParamData = requestParamData;
    }

    public String getRequestSessionId() {
        return requestSessionId;
    }

    public void setRequestSessionId(String requestSessionId) {
        this.requestSessionId = requestSessionId;
    }

    public Timestamp getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Timestamp requestTime) {
        this.requestTime = requestTime;
    }

    public String getRequestReturnTime() {
        return requestReturnTime;
    }

    public void setRequestReturnTime(String requestReturnTime) {
        this.requestReturnTime = requestReturnTime;
    }

    public String getRequestReturnData() {
        return requestReturnData;
    }

    public void setRequestReturnData(String requestReturnData) {
        this.requestReturnData = requestReturnData;
    }

    public String getRequestHttpStatusCode() {
        return requestHttpStatusCode;
    }

    public void setRequestHttpStatusCode(String requestHttpStatusCode) {
        this.requestHttpStatusCode = requestHttpStatusCode;
    }

    public int getRequestTimeConsuming() {
        return requestTimeConsuming;
    }

    public void setRequestTimeConsuming(int requestTimeConsuming) {
        this.requestTimeConsuming = requestTimeConsuming;
    }
}
