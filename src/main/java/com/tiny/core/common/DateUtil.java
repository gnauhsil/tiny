package com.tiny.core.common;

import com.tiny.core.domain.DateDomain;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by gnauhsil on 2018/6/20.
 */

@Component("dateUtil")
public class DateUtil {

    public DateDomain getDateDomain(){
        DateDomain dateDomain = new DateDomain();

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String toDay = sf.format(now);
        try {
            cal.setTime(sf.parse(toDay.toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int d = 0;
        if(cal.get(Calendar.DAY_OF_WEEK)==1){
            d = -6;
        }else{
            d = 2-cal.get(Calendar.DAY_OF_WEEK);
        }
        cal.add(Calendar.DAY_OF_WEEK, d);


        //所在周开始日期
//System.out.println(sf.format(cal.getTime()));


        dateDomain.setWeekDateStart(sf.format(cal.getTime()));

        //所在周结束日期
//System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        cal.add(Calendar.DAY_OF_WEEK, 6);
        dateDomain.setWeekDateEnd(sf.format(cal.getTime()));

        return dateDomain;
    }

    public static void main(String[] args) {
        DateUtil dateUtil = new DateUtil();
        dateUtil.getDateDomain();
    }

}
