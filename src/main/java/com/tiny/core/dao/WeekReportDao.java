package com.tiny.core.dao;

import com.tiny.core.domain.DateDomain;
import com.tiny.core.domain.User;
import com.tiny.core.domain.WeekReport;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */

@Component("weekReportDao")
public interface WeekReportDao {



    public void add(WeekReport weekReport);

    public List<WeekReport> pack(DateDomain dateDomain);

}
