package com.tiny.core.dao;

import com.tiny.core.domain.User;
import com.tiny.core.domain.WeekReport;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */

@Component("userDao")
@CacheConfig(cacheNames = "users")
public interface UserDao {

    /**
     *  1.cacheNames 设置缓存的值;key：指定缓存的 key，这是指参数 userName 值。 key可以使用spEl表达式
     *  2.sync:  @Cacheable(cacheNames="user", key="#userName",sync = true)，
     *      如果缓存中没有数据，多个线程同时访问这个方法，则只有一个方法会执行到方法，其它方法需要等待
     *      如果缓存中已经有数据，则多个线程可以同时从缓存中获取数据
     * @param userName
     * @return
     */
    @Cacheable(cacheNames="user", key="#userName")
    User findByName(@Param("userName") String userName);

    @Cacheable
    List<User> userList();

    /**
     * @CacheEvict  清空缓存
     * 涉及到 增删改、先清掉缓存，同步数据
     * @param user
     */
    @CachePut(cacheNames="user",key = "#p0.id")
    @CacheEvict(key ="#p0.id",allEntries=true)
    void add(User user);

}
