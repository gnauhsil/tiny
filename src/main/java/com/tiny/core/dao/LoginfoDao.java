package com.tiny.core.dao;

import com.tiny.core.domain.Loginfo;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */


/**
 * 事务管理：查询在 service层即可，增删改 在 dao层
 *
 * 比较  UserDao  list 方法
 */
@Component("loginfoDao")
@Transactional
public interface LoginfoDao {

    public void add(Loginfo loginfo);

}
