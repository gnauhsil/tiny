package com.tiny.core.service;

import com.tiny.core.domain.User;

import java.util.List;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */

public interface UserService {

    /**
     * 根据姓名 查询个人信息
     * @param userName
     */
    User findUserByName(String userName);

    List<User> userList();

    User userRedis(String key);

    public void add(User user);

}
