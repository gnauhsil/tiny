package com.tiny.core.service.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiny.core.common.DataSwitch;
import com.tiny.core.common.DateUtil;
import com.tiny.core.common.Document;
import com.tiny.core.controller.LogController;
import com.tiny.core.dao.WeekReportDao;
import com.tiny.core.domain.DateDomain;
import com.tiny.core.domain.WeekReport;
import com.tiny.core.service.WeekReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */
@Service
public class WeekReportServiceImpl implements WeekReportService {

    private final static Logger logger = LoggerFactory.getLogger(WeekReportServiceImpl.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    @Qualifier("weekReportDao")
    private WeekReportDao weekReportDao;

    @Autowired
    @Qualifier("doc")
    private Document doc;

    @Autowired
    @Qualifier("dataSwitch")
    private DataSwitch dataSwitch;

    @Autowired
    @Qualifier("dateUtil")
    private DateUtil dateUtil;

    private  WeekReportDao weekReportDao1;

    public WeekReportDao getWeekReportDao1() {
        return weekReportDao1;
    }

    public void setWeekReportDao1(WeekReportDao weekReportDao1) {
        this.weekReportDao1 = weekReportDao1;
    }


    /**
     * 生成个人周报
     * @param weekReport
     */
    @Override
    public void add(WeekReport weekReport) {

        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

        logger.debug("weekReportweekReportweekReport+++++++++++  "  + weekReport.getCurrentNewCorecontent());
        Map dataMap = new HashMap<String, Object>();

        weekReport.setWeekDateStart(dateUtil.getDateDomain().getWeekDateStart());
        weekReport.setWeekDateEnd(dateUtil.getDateDomain().getWeekDateEnd());

        weekReportDao.add(weekReport);
        dataMap = dataSwitch.setObjToMap(weekReport);
        dataMap.put("userName",weekReport.getUserSeq());
        doc.createDoc(dataMap, "week", "D:\\6.21-2018工作周报_核心组_"+weekReport.getUserSeq()+".doc");

    }

    /**
     * 整合本周 周报
     */
    public List<WeekReport> pack(){
        List<WeekReport> packList = null;
        DateDomain dateDomain = dateUtil.getDateDomain();
        WeekReport weekReport = new WeekReport();
        packList = weekReportDao.pack(dateDomain);
        logger.info(packList.size() + " ************* ");

        StringBuffer content = new StringBuffer();
        for(int i = 0;i < packList.size();i++){
            logger.info("packList.get(i).getCurrentNewCorecontent()  " + packList.get(i).getCurrentNewCorecontent());
            content.append(packList.get(i).getCurrentNewCorecontent());
        }

        weekReport.setCurrentNewCorecontent(content.toString());


        Map dataMap = new HashMap<String, Object>();
        weekReport.setWeekDateStart(dateUtil.getDateDomain().getWeekDateStart());
        weekReport.setWeekDateEnd(dateUtil.getDateDomain().getWeekDateEnd());
        dataMap = dataSwitch.setObjToMap(weekReport);
        dataMap.put("userName","pack");
        doc.createDoc(dataMap, "week", "D:\\week.doc");

        return packList;


    }

}
