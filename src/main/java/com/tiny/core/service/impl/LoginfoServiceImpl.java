package com.tiny.core.service.impl;

import com.tiny.core.dao.LoginfoDao;
import com.tiny.core.domain.Loginfo;
import com.tiny.core.service.LoginfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */
@Service
public class LoginfoServiceImpl implements LoginfoService {

    private final static Logger logger = LoggerFactory.getLogger(LoginfoServiceImpl.class);

    @Autowired
    @Qualifier("loginfoDao")
    private LoginfoDao loginfoDao;


    /**
     * 记录日志
     * @param loginfo
     */
    @Override
    public void add(Loginfo loginfo) {

        loginfoDao.add(loginfo);

    }

}
