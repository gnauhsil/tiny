package com.tiny.core.service;

import com.tiny.core.domain.User;
import com.tiny.core.domain.WeekReport;

import java.util.List;

/**
 * Created by connie_gnauhsil on 2018/5/30.
 */

public interface WeekReportService {

    public void add(WeekReport weekReport);

    public List<WeekReport> pack();

}
