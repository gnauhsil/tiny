<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
	<w:ignoreSubtree w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
	<o:DocumentProperties>
		<o:Author>connie_gnauhsil</o:Author>
		<o:LastAuthor>connie_gnauhsil</o:LastAuthor>
		<o:Revision>7</o:Revision>
		<o:TotalTime>2</o:TotalTime>
		<o:Created>2018-06-15T03:52:00Z</o:Created>
		<o:LastSaved>2018-06-20T05:48:00Z</o:LastSaved>
		<o:Pages>3</o:Pages>
		<o:Words>110</o:Words>
		<o:Characters>630</o:Characters>
		<o:Company>Microsoft</o:Company>
		<o:Lines>5</o:Lines>
		<o:Paragraphs>1</o:Paragraphs>
		<o:CharactersWithSpaces>739</o:CharactersWithSpaces>
		<o:Version>15</o:Version>
	</o:DocumentProperties>
	<w:fonts>
		<w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
			<w:font w:name="Times New Roman">
				<w:panose-1 w:val="02020603050405020304"/>
				<w:charset w:val="00"/>
				<w:family w:val="Roman"/>
				<w:pitch w:val="variable"/>
				<w:sig w:usb-0="E0002EFF" w:usb-1="C000785B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="宋体">
				<w:altName w:val="SimSun"/>
				<w:panose-1 w:val="02010600030101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="auto"/>
				<w:pitch w:val="variable"/>
				<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="黑体">
				<w:altName w:val="SimHei"/>
				<w:panose-1 w:val="02010609060101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="Modern"/>
				<w:pitch w:val="fixed"/>
				<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="黑体">
				<w:altName w:val="SimHei"/>
				<w:panose-1 w:val="02010609060101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="Modern"/>
				<w:pitch w:val="fixed"/>
				<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="Calibri">
				<w:panose-1 w:val="020F0502020204030204"/>
				<w:charset w:val="00"/>
				<w:family w:val="Swiss"/>
				<w:pitch w:val="variable"/>
				<w:sig w:usb-0="E0002AFF" w:usb-1="C000247B" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="仿宋">
				<w:panose-1 w:val="02010609060101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="Modern"/>
				<w:pitch w:val="fixed"/>
				<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="@宋体">
				<w:panose-1 w:val="02010600030101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="auto"/>
				<w:pitch w:val="variable"/>
				<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="@仿宋">
				<w:panose-1 w:val="02010609060101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="Modern"/>
				<w:pitch w:val="fixed"/>
				<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
			<w:font w:name="@黑体">
				<w:panose-1 w:val="02010609060101010101"/>
				<w:charset w:val="86"/>
				<w:family w:val="Modern"/>
				<w:pitch w:val="fixed"/>
				<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
			</w:font>
		</w:fonts>
		<w:lists>
			<w:listDef w:listDefId="0">
				<w:lsid w:val="226E1497"/>
				<w:plt w:val="SingleLevel"/>
				<w:tmpl w:val="590B3157"/>
				<w:lvl w:ilvl="0">
					<w:start w:val="1"/>
					<w:nfc w:val="37"/>
					<w:suff w:val="Nothing"/>
					<w:lvlText w:val="（%1）"/>
					<w:lvlJc w:val="left"/>
				</w:lvl>
			</w:listDef>
			<w:listDef w:listDefId="1">
				<w:lsid w:val="56181FE6"/>
				<w:plt w:val="SingleLevel"/>
				<w:tmpl w:val="590B3144"/>
				<w:lvl w:ilvl="0">
					<w:start w:val="1"/>
					<w:nfc w:val="37"/>
					<w:suff w:val="Nothing"/>
					<w:lvlText w:val="%1、"/>
					<w:lvlJc w:val="left"/>
				</w:lvl>
			</w:listDef>
			<w:listDef w:listDefId="2">
				<w:lsid w:val="590B3144"/>
				<w:plt w:val="SingleLevel"/>
				<w:tmpl w:val="590B3144"/>
				<w:lvl w:ilvl="0">
					<w:start w:val="1"/>
					<w:nfc w:val="37"/>
					<w:suff w:val="Nothing"/>
					<w:lvlText w:val="%1、"/>
					<w:lvlJc w:val="left"/>
				</w:lvl>
			</w:listDef>
			<w:listDef w:listDefId="3">
				<w:lsid w:val="590B3157"/>
				<w:plt w:val="SingleLevel"/>
				<w:tmpl w:val="590B3157"/>
				<w:lvl w:ilvl="0">
					<w:start w:val="1"/>
					<w:nfc w:val="37"/>
					<w:suff w:val="Nothing"/>
					<w:lvlText w:val="（%1）"/>
					<w:lvlJc w:val="left"/>
				</w:lvl>
			</w:listDef>
			<w:listDef w:listDefId="4">
				<w:lsid w:val="591445D7"/>
				<w:plt w:val="SingleLevel"/>
				<w:tmpl w:val="591445D7"/>
				<w:lvl w:ilvl="0">
					<w:start w:val="2"/>
					<w:nfc w:val="37"/>
					<w:suff w:val="Nothing"/>
					<w:lvlText w:val="（%1）"/>
					<w:lvlJc w:val="left"/>
				</w:lvl>
			</w:listDef>
			<w:list w:ilfo="1">
				<w:ilst w:val="2"/>
				<w:lvlOverride w:ilvl="0">
					<w:startOverride w:val="1"/>
				</w:lvlOverride>
			</w:list>
			<w:list w:ilfo="2">
				<w:ilst w:val="3"/>
				<w:lvlOverride w:ilvl="0">
					<w:startOverride w:val="1"/>
				</w:lvlOverride>
			</w:list>
			<w:list w:ilfo="3">
				<w:ilst w:val="4"/>
			</w:list>
			<w:list w:ilfo="4">
				<w:ilst w:val="1"/>
			</w:list>
			<w:list w:ilfo="5">
				<w:ilst w:val="0"/>
			</w:list>
		</w:lists>
		<w:styles>
			<w:versionOfBuiltInStylenames w:val="7"/>
			<w:latentStyles w:defLockedState="off" w:latentStyleCount="371">
				<w:lsdException w:name="Normal"/>
				<w:lsdException w:name="heading 1"/>
				<w:lsdException w:name="heading 2"/>
				<w:lsdException w:name="heading 3"/>
				<w:lsdException w:name="heading 4"/>
				<w:lsdException w:name="heading 5"/>
				<w:lsdException w:name="heading 6"/>
				<w:lsdException w:name="heading 7"/>
				<w:lsdException w:name="heading 8"/>
				<w:lsdException w:name="heading 9"/>
				<w:lsdException w:name="caption"/>
				<w:lsdException w:name="Title"/>
				<w:lsdException w:name="Subtitle"/>
				<w:lsdException w:name="Strong"/>
				<w:lsdException w:name="Emphasis"/>
				<w:lsdException w:name="Normal Table"/>
				<w:lsdException w:name="Table Simple 1"/>
				<w:lsdException w:name="Table Simple 2"/>
				<w:lsdException w:name="Table Simple 3"/>
				<w:lsdException w:name="Table Classic 1"/>
				<w:lsdException w:name="Table Classic 2"/>
				<w:lsdException w:name="Table Classic 3"/>
				<w:lsdException w:name="Table Classic 4"/>
				<w:lsdException w:name="Table Colorful 1"/>
				<w:lsdException w:name="Table Colorful 2"/>
				<w:lsdException w:name="Table Colorful 3"/>
				<w:lsdException w:name="Table Columns 1"/>
				<w:lsdException w:name="Table Columns 2"/>
				<w:lsdException w:name="Table Columns 3"/>
				<w:lsdException w:name="Table Columns 4"/>
				<w:lsdException w:name="Table Columns 5"/>
				<w:lsdException w:name="Table Grid 1"/>
				<w:lsdException w:name="Table Grid 2"/>
				<w:lsdException w:name="Table Grid 3"/>
				<w:lsdException w:name="Table Grid 4"/>
				<w:lsdException w:name="Table Grid 5"/>
				<w:lsdException w:name="Table Grid 6"/>
				<w:lsdException w:name="Table Grid 7"/>
				<w:lsdException w:name="Table Grid 8"/>
				<w:lsdException w:name="Table List 1"/>
				<w:lsdException w:name="Table List 2"/>
				<w:lsdException w:name="Table List 3"/>
				<w:lsdException w:name="Table List 4"/>
				<w:lsdException w:name="Table List 5"/>
				<w:lsdException w:name="Table List 6"/>
				<w:lsdException w:name="Table List 7"/>
				<w:lsdException w:name="Table List 8"/>
				<w:lsdException w:name="Table 3D effects 1"/>
				<w:lsdException w:name="Table 3D effects 2"/>
				<w:lsdException w:name="Table 3D effects 3"/>
				<w:lsdException w:name="Table Contemporary"/>
				<w:lsdException w:name="Table Elegant"/>
				<w:lsdException w:name="Table Professional"/>
				<w:lsdException w:name="Table Subtle 1"/>
				<w:lsdException w:name="Table Subtle 2"/>
				<w:lsdException w:name="Table Web 1"/>
				<w:lsdException w:name="Table Web 2"/>
				<w:lsdException w:name="Table Web 3"/>
				<w:lsdException w:name="Table Theme"/>
				<w:lsdException w:name="No Spacing"/>
				<w:lsdException w:name="Light Shading"/>
				<w:lsdException w:name="Light List"/>
				<w:lsdException w:name="Light Grid"/>
				<w:lsdException w:name="Medium Shading 1"/>
				<w:lsdException w:name="Medium Shading 2"/>
				<w:lsdException w:name="Medium List 1"/>
				<w:lsdException w:name="Medium List 2"/>
				<w:lsdException w:name="Medium Grid 1"/>
				<w:lsdException w:name="Medium Grid 2"/>
				<w:lsdException w:name="Medium Grid 3"/>
				<w:lsdException w:name="Dark List"/>
				<w:lsdException w:name="Colorful Shading"/>
				<w:lsdException w:name="Colorful List"/>
				<w:lsdException w:name="Colorful Grid"/>
				<w:lsdException w:name="Light Shading Accent 1"/>
				<w:lsdException w:name="Light List Accent 1"/>
				<w:lsdException w:name="Light Grid Accent 1"/>
				<w:lsdException w:name="Medium Shading 1 Accent 1"/>
				<w:lsdException w:name="Medium Shading 2 Accent 1"/>
				<w:lsdException w:name="Medium List 1 Accent 1"/>
				<w:lsdException w:name="List Paragraph"/>
				<w:lsdException w:name="Quote"/>
				<w:lsdException w:name="Intense Quote"/>
				<w:lsdException w:name="Medium List 2 Accent 1"/>
				<w:lsdException w:name="Medium Grid 1 Accent 1"/>
				<w:lsdException w:name="Medium Grid 2 Accent 1"/>
				<w:lsdException w:name="Medium Grid 3 Accent 1"/>
				<w:lsdException w:name="Dark List Accent 1"/>
				<w:lsdException w:name="Colorful Shading Accent 1"/>
				<w:lsdException w:name="Colorful List Accent 1"/>
				<w:lsdException w:name="Colorful Grid Accent 1"/>
				<w:lsdException w:name="Light Shading Accent 2"/>
				<w:lsdException w:name="Light List Accent 2"/>
				<w:lsdException w:name="Light Grid Accent 2"/>
				<w:lsdException w:name="Medium Shading 1 Accent 2"/>
				<w:lsdException w:name="Medium Shading 2 Accent 2"/>
				<w:lsdException w:name="Medium List 1 Accent 2"/>
				<w:lsdException w:name="Medium List 2 Accent 2"/>
				<w:lsdException w:name="Medium Grid 1 Accent 2"/>
				<w:lsdException w:name="Medium Grid 2 Accent 2"/>
				<w:lsdException w:name="Medium Grid 3 Accent 2"/>
				<w:lsdException w:name="Dark List Accent 2"/>
				<w:lsdException w:name="Colorful Shading Accent 2"/>
				<w:lsdException w:name="Colorful List Accent 2"/>
				<w:lsdException w:name="Colorful Grid Accent 2"/>
				<w:lsdException w:name="Light Shading Accent 3"/>
				<w:lsdException w:name="Light List Accent 3"/>
				<w:lsdException w:name="Light Grid Accent 3"/>
				<w:lsdException w:name="Medium Shading 1 Accent 3"/>
				<w:lsdException w:name="Medium Shading 2 Accent 3"/>
				<w:lsdException w:name="Medium List 1 Accent 3"/>
				<w:lsdException w:name="Medium List 2 Accent 3"/>
				<w:lsdException w:name="Medium Grid 1 Accent 3"/>
				<w:lsdException w:name="Medium Grid 2 Accent 3"/>
				<w:lsdException w:name="Medium Grid 3 Accent 3"/>
				<w:lsdException w:name="Dark List Accent 3"/>
				<w:lsdException w:name="Colorful Shading Accent 3"/>
				<w:lsdException w:name="Colorful List Accent 3"/>
				<w:lsdException w:name="Colorful Grid Accent 3"/>
				<w:lsdException w:name="Light Shading Accent 4"/>
				<w:lsdException w:name="Light List Accent 4"/>
				<w:lsdException w:name="Light Grid Accent 4"/>
				<w:lsdException w:name="Medium Shading 1 Accent 4"/>
				<w:lsdException w:name="Medium Shading 2 Accent 4"/>
				<w:lsdException w:name="Medium List 1 Accent 4"/>
				<w:lsdException w:name="Medium List 2 Accent 4"/>
				<w:lsdException w:name="Medium Grid 1 Accent 4"/>
				<w:lsdException w:name="Medium Grid 2 Accent 4"/>
				<w:lsdException w:name="Medium Grid 3 Accent 4"/>
				<w:lsdException w:name="Dark List Accent 4"/>
				<w:lsdException w:name="Colorful Shading Accent 4"/>
				<w:lsdException w:name="Colorful List Accent 4"/>
				<w:lsdException w:name="Colorful Grid Accent 4"/>
				<w:lsdException w:name="Light Shading Accent 5"/>
				<w:lsdException w:name="Light List Accent 5"/>
				<w:lsdException w:name="Light Grid Accent 5"/>
				<w:lsdException w:name="Medium Shading 1 Accent 5"/>
				<w:lsdException w:name="Medium Shading 2 Accent 5"/>
				<w:lsdException w:name="Medium List 1 Accent 5"/>
				<w:lsdException w:name="Medium List 2 Accent 5"/>
				<w:lsdException w:name="Medium Grid 1 Accent 5"/>
				<w:lsdException w:name="Medium Grid 2 Accent 5"/>
				<w:lsdException w:name="Medium Grid 3 Accent 5"/>
				<w:lsdException w:name="Dark List Accent 5"/>
				<w:lsdException w:name="Colorful Shading Accent 5"/>
				<w:lsdException w:name="Colorful List Accent 5"/>
				<w:lsdException w:name="Colorful Grid Accent 5"/>
				<w:lsdException w:name="Light Shading Accent 6"/>
				<w:lsdException w:name="Light List Accent 6"/>
				<w:lsdException w:name="Light Grid Accent 6"/>
				<w:lsdException w:name="Medium Shading 1 Accent 6"/>
				<w:lsdException w:name="Medium Shading 2 Accent 6"/>
				<w:lsdException w:name="Medium List 1 Accent 6"/>
				<w:lsdException w:name="Medium List 2 Accent 6"/>
				<w:lsdException w:name="Medium Grid 1 Accent 6"/>
				<w:lsdException w:name="Medium Grid 2 Accent 6"/>
				<w:lsdException w:name="Medium Grid 3 Accent 6"/>
				<w:lsdException w:name="Dark List Accent 6"/>
				<w:lsdException w:name="Colorful Shading Accent 6"/>
				<w:lsdException w:name="Colorful List Accent 6"/>
				<w:lsdException w:name="Colorful Grid Accent 6"/>
				<w:lsdException w:name="Subtle Emphasis"/>
				<w:lsdException w:name="Intense Emphasis"/>
				<w:lsdException w:name="Subtle Reference"/>
				<w:lsdException w:name="Intense Reference"/>
				<w:lsdException w:name="Book Title"/>
				<w:lsdException w:name="TOC Heading"/>
			</w:latentStyles>
			<w:style w:type="paragraph" w:default="on" w:styleId="a">
				<w:name w:val="Normal"/>
				<wx:uiName wx:val="正文"/>
				<w:rsid w:val="008E760D"/>
				<w:pPr>
					<w:widowControl w:val="off"/>
					<w:jc w:val="both"/>
				</w:pPr>
				<w:rPr>
					<wx:font wx:val="Calibri"/>
					<w:kern w:val="2"/>
					<w:sz w:val="21"/>
					<w:sz-cs w:val="22"/>
					<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
				</w:rPr>
			</w:style>
			<w:style w:type="character" w:default="on" w:styleId="a0">
				<w:name w:val="Default Paragraph Font"/>
				<wx:uiName wx:val="默认段落字体"/>
			</w:style>
			<w:style w:type="table" w:default="on" w:styleId="a1">
				<w:name w:val="Normal Table"/>
				<wx:uiName wx:val="普通表格"/>
				<w:rPr>
					<wx:font wx:val="Calibri"/>
					<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
				</w:rPr>
				<w:tblPr>
					<w:tblInd w:w="0" w:type="dxa"/>
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa"/>
						<w:left w:w="108" w:type="dxa"/>
						<w:bottom w:w="0" w:type="dxa"/>
						<w:right w:w="108" w:type="dxa"/>
					</w:tblCellMar>
				</w:tblPr>
			</w:style>
			<w:style w:type="list" w:default="on" w:styleId="a2">
				<w:name w:val="No List"/>
				<wx:uiName wx:val="无列表"/>
			</w:style>
			<w:style w:type="paragraph" w:styleId="a3">
				<w:name w:val="header"/>
				<wx:uiName wx:val="页眉"/>
				<w:basedOn w:val="a"/>
				<w:link w:val="Char"/>
				<w:rsid w:val="009B14BD"/>
				<w:pPr>
					<w:pBdr>
						<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
					</w:pBdr>
					<w:tabs>
						<w:tab w:val="center" w:pos="4153"/>
						<w:tab w:val="right" w:pos="8306"/>
					</w:tabs>
					<w:snapToGrid w:val="off"/>
					<w:jc w:val="center"/>
				</w:pPr>
				<w:rPr>
					<wx:font wx:val="Calibri"/>
					<w:sz w:val="18"/>
					<w:sz-cs w:val="18"/>
				</w:rPr>
			</w:style>
			<w:style w:type="character" w:styleId="Char">
				<w:name w:val="页眉 Char"/>
				<w:link w:val="a3"/>
				<w:rsid w:val="009B14BD"/>
				<w:rPr>
					<w:sz w:val="18"/>
					<w:sz-cs w:val="18"/>
				</w:rPr>
			</w:style>
			<w:style w:type="paragraph" w:styleId="a4">
				<w:name w:val="footer"/>
				<wx:uiName wx:val="页脚"/>
				<w:basedOn w:val="a"/>
				<w:link w:val="Char0"/>
				<w:rsid w:val="009B14BD"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="center" w:pos="4153"/>
						<w:tab w:val="right" w:pos="8306"/>
					</w:tabs>
					<w:snapToGrid w:val="off"/>
					<w:jc w:val="left"/>
				</w:pPr>
				<w:rPr>
					<wx:font wx:val="Calibri"/>
					<w:sz w:val="18"/>
					<w:sz-cs w:val="18"/>
				</w:rPr>
			</w:style>
			<w:style w:type="character" w:styleId="Char0">
				<w:name w:val="页脚 Char"/>
				<w:link w:val="a4"/>
				<w:rsid w:val="009B14BD"/>
				<w:rPr>
					<w:sz w:val="18"/>
					<w:sz-cs w:val="18"/>
				</w:rPr>
			</w:style>
		</w:styles>
		<w:divs>
			<w:div w:id="1379430100">
				<w:bodyDiv w:val="on"/>
				<w:marLeft w:val="0"/>
				<w:marRight w:val="0"/>
				<w:marTop w:val="0"/>
				<w:marBottom w:val="0"/>
				<w:divBdr>
					<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
					<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
					<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
					<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				</w:divBdr>
			</w:div>
		</w:divs>
		<w:shapeDefaults>
			<o:shapedefaults v:ext="edit" spidmax="2049"/>
			<o:shapelayout v:ext="edit">
				<o:idmap v:ext="edit" data="1"/>
			</o:shapelayout>
		</w:shapeDefaults>
		<w:docPr>
			<w:view w:val="print"/>
			<w:zoom w:percent="120"/>
			<w:doNotEmbedSystemFonts/>
			<w:bordersDontSurroundHeader/>
			<w:bordersDontSurroundFooter/>
			<w:defaultTabStop w:val="420"/>
			<w:drawingGridVerticalSpacing w:val="156"/>
			<w:displayHorizontalDrawingGridEvery w:val="0"/>
			<w:displayVerticalDrawingGridEvery w:val="2"/>
			<w:punctuationKerning/>
			<w:characterSpacingControl w:val="CompressPunctuation"/>
			<w:optimizeForBrowser/>
			<w:allowPNG/>
			<w:validateAgainstSchema/>
			<w:saveInvalidXML w:val="off"/>
			<w:ignoreMixedContent w:val="off"/>
			<w:alwaysShowPlaceholderText w:val="off"/>
			<w:hdrShapeDefaults>
				<o:shapedefaults v:ext="edit" spidmax="2049"/>
			</w:hdrShapeDefaults>
			<w:footnotePr>
				<w:footnote w:type="separator">
					<w:p wsp:rsidR="00FF2044" wsp:rsidRDefault="00FF2044" wsp:rsidP="009B14BD">
						<w:r>
							<w:separator/>
						</w:r>
					</w:p>
				</w:footnote>
				<w:footnote w:type="continuation-separator">
					<w:p wsp:rsidR="00FF2044" wsp:rsidRDefault="00FF2044" wsp:rsidP="009B14BD">
						<w:r>
							<w:continuationSeparator/>
						</w:r>
					</w:p>
				</w:footnote>
			</w:footnotePr>
			<w:endnotePr>
				<w:endnote w:type="separator">
					<w:p wsp:rsidR="00FF2044" wsp:rsidRDefault="00FF2044" wsp:rsidP="009B14BD">
						<w:r>
							<w:separator/>
						</w:r>
					</w:p>
				</w:endnote>
				<w:endnote w:type="continuation-separator">
					<w:p wsp:rsidR="00FF2044" wsp:rsidRDefault="00FF2044" wsp:rsidP="009B14BD">
						<w:r>
							<w:continuationSeparator/>
						</w:r>
					</w:p>
				</w:endnote>
			</w:endnotePr>
			<w:compat>
				<w:spaceForUL/>
				<w:balanceSingleByteDoubleByteWidth/>
				<w:doNotLeaveBackslashAlone/>
				<w:ulTrailSpace/>
				<w:doNotExpandShiftReturn/>
				<w:adjustLineHeightInTable/>
				<w:breakWrappedTables/>
				<w:snapToGridInCell/>
				<w:wrapTextWithPunct/>
				<w:useAsianBreakRules/>
				<w:dontGrowAutofit/>
				<w:useFELayout/>
			</w:compat>
			<wsp:rsids>
				<wsp:rsidRoot wsp:val="002364E6"/>
				<wsp:rsid wsp:val="000A474C"/>
				<wsp:rsid wsp:val="000D515D"/>
				<wsp:rsid wsp:val="00203A16"/>
				<wsp:rsid wsp:val="002364E6"/>
				<wsp:rsid wsp:val="002A491F"/>
				<wsp:rsid wsp:val="005658C3"/>
				<wsp:rsid wsp:val="00802845"/>
				<wsp:rsid wsp:val="008C2117"/>
				<wsp:rsid wsp:val="008E760D"/>
				<wsp:rsid wsp:val="00986A3C"/>
				<wsp:rsid wsp:val="009B14BD"/>
				<wsp:rsid wsp:val="00A445D7"/>
				<wsp:rsid wsp:val="00B557F5"/>
				<wsp:rsid wsp:val="00C87A69"/>
				<wsp:rsid wsp:val="00E256FB"/>
				<wsp:rsid wsp:val="00ED0936"/>
				<wsp:rsid wsp:val="00FF2044"/>
			</wsp:rsids>
		</w:docPr>
		<w:body>
			<wx:sect>
				<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
					<w:pPr>
						<w:spacing w:line="560" w:line-rule="exact"/>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:sz w:val="36"/>
							<w:sz-cs w:val="36"/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00E256FB">
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
								<wx:font wx:val="宋体"/>
								<w:b/>
								<w:sz w:val="36"/>
								<w:sz-cs w:val="36"/>
							</w:rPr>
							<w:t>软件中心</w:t>
						</w:r>
						<w:r wsp:rsidRPr="00E256FB">
							<w:rPr>
								<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
									<wx:font wx:val="宋体"/>
									<w:b/>
									<w:sz w:val="36"/>
									<w:sz-cs w:val="36"/>
									<w:u w:val="single"/>
								</w:rPr>
								<w:t>   ${userName}  </w:t>
							</w:r>
							<w:r wsp:rsidRPr="00E256FB">
								<w:rPr>
									<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:b/>
										<w:sz w:val="36"/>
										<w:sz-cs w:val="36"/>
									</w:rPr>
									<w:t>工作周报</w:t>
								</w:r>
							</w:p>
							<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
								<w:pPr>
									<w:spacing w:line="560" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
											<wx:font wx:val="仿宋"/>
											<w:sz w:val="28"/>
											<w:sz-cs w:val="28"/>
										</w:rPr>
									</w:pPr>
									<w:r wsp:rsidRPr="00E256FB">
										<w:rPr>
											<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
												<wx:font wx:val="仿宋"/>
												<w:sz w:val="28"/>
												<w:sz-cs w:val="28"/>
											</w:rPr>
											<w:t>（</w:t>
										</w:r>
										<w:r wsp:rsidR="00802845" wsp:rsidRPr="00802845">
											<w:rPr>
												<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
													<wx:font wx:val="仿宋"/>
													<w:sz w:val="28"/>
													<w:sz-cs w:val="28"/>
												</w:rPr>
												<w:t>${weekDateStart}</w:t>
														</w:r>
														<w:r wsp:rsidRPr="00E256FB">
															<w:rPr>
																<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																	<wx:font wx:val="仿宋"/>
																	<w:sz w:val="28"/>
																	<w:sz-cs w:val="28"/>
																</w:rPr>
																<w:t> - </w:t>
															</w:r>
															<w:r wsp:rsidR="00C87A69" wsp:rsidRPr="00C87A69">
																<w:rPr>
																	<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																		<wx:font wx:val="仿宋"/>
																		<w:sz w:val="28"/>
																		<w:sz-cs w:val="28"/>
																	</w:rPr>
																	<w:t>${weekDateEnd}</w:t>
																		</w:r>
																		<w:r wsp:rsidRPr="00E256FB">
																			<w:rPr>
																				<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																					<wx:font wx:val="仿宋"/>
																					<w:sz w:val="28"/>
																					<w:sz-cs w:val="28"/>
																				</w:rPr>
																				<w:t>）</w:t>
																			</w:r>
																		</w:p>
																		<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																			<w:pPr>
																				<w:spacing w:line="560" w:line-rule="exact"/>
																				<w:rPr>
																					<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体"/>
																						<wx:font wx:val="黑体"/>
																						<w:sz w:val="30"/>
																					</w:rPr>
																				</w:pPr>
																				<w:r wsp:rsidRPr="00E256FB">
																					<w:rPr>
																						<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
																							<wx:font wx:val="黑体"/>
																							<w:sz w:val="30"/>
																						</w:rPr>
																						<w:t>本周工作完成情况：</w:t>
																					</w:r>
																				</w:p>
																				<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																					<w:pPr>
																						<w:listPr>
																							<w:ilvl w:val="0"/>
																							<w:ilfo w:val="1"/>
																							<wx:t wx:val="一、"/>
																							<wx:font wx:val="Times New Roman"/>
																						</w:listPr>
																						<w:spacing w:line="560" w:line-rule="exact"/>
																						<w:rPr>
																							<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																								<wx:font wx:val="仿宋"/>
																								<w:b/>
																								<w:sz w:val="30"/>
																								<w:sz-cs w:val="30"/>
																							</w:rPr>
																						</w:pPr>
																						<w:r wsp:rsidRPr="00E256FB">
																							<w:rPr>
																								<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																									<wx:font wx:val="仿宋"/>
																									<w:b/>
																									<w:sz w:val="30"/>
																									<w:sz-cs w:val="30"/>
																								</w:rPr>
																								<w:t>重点工作进展情况</w:t>
																							</w:r>
																						</w:p>
																						<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																							<w:pPr>
																								<w:listPr>
																									<w:ilvl w:val="0"/>
																									<w:ilfo w:val="2"/>
																									<wx:t wx:val="（一）"/>
																									<wx:font wx:val="Times New Roman"/>
																								</w:listPr>
																								<w:spacing w:line="560" w:line-rule="exact"/>
																								<w:rPr>
																									<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																										<wx:font wx:val="仿宋"/>
																										<w:b/>
																										<w:sz w:val="30"/>
																										<w:sz-cs w:val="30"/>
																									</w:rPr>
																								</w:pPr>
																								<w:r wsp:rsidRPr="00E256FB">
																									<w:rPr>
																										<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																											<wx:font wx:val="仿宋"/>
																											<w:b/>
																											<w:sz w:val="30"/>
																											<w:sz-cs w:val="30"/>
																										</w:rPr>
																										<w:t>新核心项目群</w:t>
																									</w:r>
																								</w:p>
																								<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																									<w:pPr>
																										<w:spacing w:line="560" w:line-rule="exact"/>
																										<w:ind w:first-line-chars="100" w:first-line="361"/>
																										<w:rPr>
																											<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																											<wx:font wx:val="宋体"/>
																											<w:b/>
																											<w:sz w:val="36"/>
																											<w:sz-cs w:val="36"/>
																										</w:rPr>
																									</w:pPr>
																									<w:r wsp:rsidRPr="00E256FB">
																										<w:rPr>
																											<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																												<wx:font wx:val="宋体"/>
																												<w:b/>
																												<w:sz w:val="36"/>
																												<w:sz-cs w:val="36"/>
																											</w:rPr>
																											<w:t>${currentNewCorecontent}</w:t>
																										</w:r>
																									</w:p>
																									<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																										<w:pPr>
																											<w:listPr>
																												<w:ilvl w:val="0"/>
																												<w:ilfo w:val="2"/>
																												<wx:t wx:val="（二）"/>
																												<wx:font wx:val="Times New Roman"/>
																											</w:listPr>
																											<w:spacing w:line="560" w:line-rule="exact"/>
																											<w:rPr>
																												<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																													<wx:font wx:val="仿宋"/>
																													<w:b/>
																													<w:sz w:val="30"/>
																													<w:sz-cs w:val="30"/>
																												</w:rPr>
																											</w:pPr>
																											<w:r wsp:rsidRPr="00E256FB">
																												<w:rPr>
																													<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																														<wx:font wx:val="仿宋"/>
																														<w:b/>
																														<w:sz w:val="30"/>
																														<w:sz-cs w:val="30"/>
																													</w:rPr>
																													<w:t>两地三中心</w:t>
																												</w:r>
																											</w:p>
																											<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																												<w:pPr>
																													<w:spacing w:line="560" w:line-rule="exact"/>
																													<w:ind w:first-line-chars="100" w:first-line="361"/>
																													<w:rPr>
																														<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																															<wx:font wx:val="仿宋"/>
																															<w:b/>
																															<w:sz w:val="30"/>
																															<w:sz-cs w:val="30"/>
																														</w:rPr>
																													</w:pPr>
																													<w:r wsp:rsidRPr="00E256FB">
																														<w:rPr>
																															<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																<wx:font wx:val="宋体"/>
																																<w:b/>
																																<w:sz w:val="36"/>
																																<w:sz-cs w:val="36"/>
																															</w:rPr>
																															<w:t>${currentTwoSecond}</w:t>
																														</w:r>
																													</w:p>
																													<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																														<w:pPr>
																															<w:listPr>
																																<w:ilvl w:val="0"/>
																																<w:ilfo w:val="1"/>
																																<wx:t wx:val="二、"/>
																																<wx:font wx:val="Times New Roman"/>
																															</w:listPr>
																															<w:spacing w:line="560" w:line-rule="exact"/>
																															<w:rPr>
																																<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																	<wx:font wx:val="仿宋"/>
																																	<w:b/>
																																	<w:sz w:val="30"/>
																																	<w:sz-cs w:val="30"/>
																																</w:rPr>
																															</w:pPr>
																															<w:r wsp:rsidRPr="00E256FB">
																																<w:rPr>
																																	<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																		<wx:font wx:val="仿宋"/>
																																		<w:b/>
																																		<w:sz w:val="30"/>
																																		<w:sz-cs w:val="30"/>
																																	</w:rPr>
																																	<w:t>基础管理工作</w:t>
																																</w:r>
																															</w:p>
																															<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																<w:pPr>
																																	<w:spacing w:line="560" w:line-rule="exact"/>
																																	<w:ind w:first-line-chars="100" w:first-line="361"/>
																																	<w:rPr>
																																		<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																		<wx:font wx:val="宋体"/>
																																		<w:b/>
																																		<w:sz w:val="36"/>
																																		<w:sz-cs w:val="36"/>
																																	</w:rPr>
																																</w:pPr>
																																<w:r wsp:rsidRPr="00E256FB">
																																	<w:rPr>
																																		<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																			<wx:font wx:val="宋体"/>
																																			<w:b/>
																																			<w:sz w:val="36"/>
																																			<w:sz-cs w:val="36"/>
																																		</w:rPr>
																																		<w:t>${currentBasicManage}</w:t>
																																	</w:r>
																																</w:p>
																																<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																	<w:pPr>
																																		<w:listPr>
																																			<w:ilvl w:val="0"/>
																																			<w:ilfo w:val="1"/>
																																			<wx:t wx:val="三、"/>
																																			<wx:font wx:val="Times New Roman"/>
																																		</w:listPr>
																																		<w:spacing w:line="560" w:line-rule="exact"/>
																																		<w:rPr>
																																			<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																				<wx:font wx:val="仿宋"/>
																																				<w:b/>
																																				<w:sz w:val="30"/>
																																				<w:sz-cs w:val="30"/>
																																			</w:rPr>
																																		</w:pPr>
																																		<w:r wsp:rsidRPr="00E256FB">
																																			<w:rPr>
																																				<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																					<wx:font wx:val="仿宋"/>
																																					<w:b/>
																																					<w:sz w:val="30"/>
																																					<w:sz-cs w:val="30"/>
																																				</w:rPr>
																																				<w:t>项目建设情况</w:t>
																																			</w:r>
																																		</w:p>
																																		<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																			<w:pPr>
																																				<w:spacing w:line="560" w:line-rule="exact"/>
																																				<w:rPr>
																																					<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																						<wx:font wx:val="仿宋"/>
																																						<w:b/>
																																						<w:sz w:val="30"/>
																																						<w:sz-cs w:val="30"/>
																																					</w:rPr>
																																				</w:pPr>
																																				<w:r wsp:rsidRPr="00E256FB">
																																					<w:rPr>
																																						<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																							<wx:font wx:val="仿宋"/>
																																							<w:b/>
																																							<w:sz w:val="30"/>
																																							<w:sz-cs w:val="30"/>
																																						</w:rPr>
																																						<w:t>（一）上线投产情况</w:t>
																																					</w:r>
																																				</w:p>
																																				<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																					<w:pPr>
																																						<w:spacing w:line="560" w:line-rule="exact"/>
																																						<w:ind w:left-chars="200" w:left="420"/>
																																						<w:rPr>
																																							<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																								<wx:font wx:val="仿宋"/>
																																								<w:sz w:val="30"/>
																																								<w:sz-cs w:val="30"/>
																																							</w:rPr>
																																						</w:pPr>
																																						<w:r wsp:rsidRPr="00E256FB">
																																							<w:rPr>
																																								<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																									<wx:font wx:val="仿宋"/>
																																									<w:sz w:val="30"/>
																																									<w:sz-cs w:val="30"/>
																																								</w:rPr>
																																								<w:t>1.项目方面</w:t>
																																							</w:r>
																																						</w:p>
																																						<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																							<w:pPr>
																																								<w:spacing w:line="560" w:line-rule="exact"/>
																																								<w:ind w:first-line-chars="216" w:first-line="781"/>
																																								<w:rPr>
																																									<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																									<wx:font wx:val="宋体"/>
																																									<w:b/>
																																									<w:sz w:val="36"/>
																																									<w:sz-cs w:val="36"/>
																																								</w:rPr>
																																							</w:pPr>
																																							<w:r wsp:rsidRPr="00E256FB">
																																								<w:rPr>
																																									<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																										<wx:font wx:val="宋体"/>
																																										<w:b/>
																																										<w:sz w:val="36"/>
																																										<w:sz-cs w:val="36"/>
																																									</w:rPr>
																																									<w:t>${currentProjectUpdate}</w:t>
																																								</w:r>
																																							</w:p>
																																							<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																								<w:pPr>
																																									<w:spacing w:line="560" w:line-rule="exact"/>
																																									<w:ind w:left-chars="200" w:left="420"/>
																																									<w:rPr>
																																										<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																											<wx:font wx:val="仿宋"/>
																																											<w:sz w:val="30"/>
																																											<w:sz-cs w:val="30"/>
																																										</w:rPr>
																																									</w:pPr>
																																									<w:r wsp:rsidRPr="00E256FB">
																																										<w:rPr>
																																											<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																												<wx:font wx:val="仿宋"/>
																																												<w:sz w:val="30"/>
																																												<w:sz-cs w:val="30"/>
																																											</w:rPr>
																																											<w:t>2.需求方面</w:t>
																																										</w:r>
																																									</w:p>
																																									<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																										<w:pPr>
																																											<w:spacing w:line="560" w:line-rule="exact"/>
																																											<w:ind w:first-line-chars="216" w:first-line="781"/>
																																											<w:rPr>
																																												<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																												<wx:font wx:val="宋体"/>
																																												<w:b/>
																																												<w:sz w:val="36"/>
																																												<w:sz-cs w:val="36"/>
																																											</w:rPr>
																																										</w:pPr>
																																										<w:r wsp:rsidRPr="00E256FB">
																																											<w:rPr>
																																												<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																													<wx:font wx:val="宋体"/>
																																													<w:b/>
																																													<w:sz w:val="36"/>
																																													<w:sz-cs w:val="36"/>
																																												</w:rPr>
																																												<w:t>${currentDemand}</w:t>
																																											</w:r>
																																										</w:p>
																																										<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																											<w:pPr>
																																												<w:listPr>
																																													<w:ilvl w:val="0"/>
																																													<w:ilfo w:val="3"/>
																																													<wx:t wx:val="（二）"/>
																																													<wx:font wx:val="Times New Roman"/>
																																												</w:listPr>
																																												<w:spacing w:line="560" w:line-rule="exact"/>
																																												<w:rPr>
																																													<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																														<wx:font wx:val="仿宋"/>
																																														<w:b/>
																																														<w:sz w:val="30"/>
																																														<w:sz-cs w:val="30"/>
																																													</w:rPr>
																																												</w:pPr>
																																												<w:r wsp:rsidRPr="00E256FB">
																																													<w:rPr>
																																														<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																															<wx:font wx:val="仿宋"/>
																																															<w:b/>
																																															<w:sz w:val="30"/>
																																															<w:sz-cs w:val="30"/>
																																														</w:rPr>
																																														<w:t>招标进展情况</w:t>
																																													</w:r>
																																												</w:p>
																																												<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																													<w:pPr>
																																														<w:spacing w:line="560" w:line-rule="exact"/>
																																														<w:ind w:left="420" w:first-line-chars="100" w:first-line="361"/>
																																														<w:rPr>
																																															<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																															<wx:font wx:val="宋体"/>
																																															<w:b/>
																																															<w:sz w:val="36"/>
																																															<w:sz-cs w:val="36"/>
																																														</w:rPr>
																																													</w:pPr>
																																													<w:r wsp:rsidRPr="00E256FB">
																																														<w:rPr>
																																															<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																<wx:font wx:val="宋体"/>
																																																<w:b/>
																																																<w:sz w:val="36"/>
																																																<w:sz-cs w:val="36"/>
																																															</w:rPr>
																																															<w:t>${currentBid}</w:t>
																																														</w:r>
																																													</w:p>
																																													<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																														<w:pPr>
																																															<w:listPr>
																																																<w:ilvl w:val="0"/>
																																																<w:ilfo w:val="3"/>
																																																<wx:t wx:val="（三）"/>
																																																<wx:font wx:val="Times New Roman"/>
																																															</w:listPr>
																																															<w:spacing w:line="560" w:line-rule="exact"/>
																																															<w:rPr>
																																																<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																	<wx:font wx:val="仿宋"/>
																																																	<w:b/>
																																																	<w:sz w:val="30"/>
																																																	<w:sz-cs w:val="30"/>
																																																</w:rPr>
																																															</w:pPr>
																																															<w:r wsp:rsidRPr="00E256FB">
																																																<w:rPr>
																																																	<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																		<wx:font wx:val="仿宋"/>
																																																		<w:b/>
																																																		<w:sz w:val="30"/>
																																																		<w:sz-cs w:val="30"/>
																																																	</w:rPr>
																																																	<w:t>商务进展情况</w:t>
																																																</w:r>
																																															</w:p>
																																															<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																<w:pPr>
																																																	<w:spacing w:line="560" w:line-rule="exact"/>
																																																	<w:ind w:first-line-chars="200" w:first-line="723"/>
																																																	<w:rPr>
																																																		<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																		<wx:font wx:val="宋体"/>
																																																		<w:b/>
																																																		<w:sz w:val="36"/>
																																																		<w:sz-cs w:val="36"/>
																																																	</w:rPr>
																																																</w:pPr>
																																																<w:r wsp:rsidRPr="00E256FB">
																																																	<w:rPr>
																																																		<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																			<wx:font wx:val="宋体"/>
																																																			<w:b/>
																																																			<w:sz w:val="36"/>
																																																			<w:sz-cs w:val="36"/>
																																																		</w:rPr>
																																																		<w:t>${currentBusiness}</w:t>
																																																	</w:r>
																																																</w:p>
																																																<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																	<w:pPr>
																																																		<w:listPr>
																																																			<w:ilvl w:val="0"/>
																																																			<w:ilfo w:val="3"/>
																																																			<wx:t wx:val="（四）"/>
																																																			<wx:font wx:val="Times New Roman"/>
																																																		</w:listPr>
																																																		<w:spacing w:line="560" w:line-rule="exact"/>
																																																		<w:rPr>
																																																			<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																				<wx:font wx:val="仿宋"/>
																																																				<w:b/>
																																																				<w:sz w:val="30"/>
																																																				<w:sz-cs w:val="30"/>
																																																			</w:rPr>
																																																		</w:pPr>
																																																		<w:r wsp:rsidRPr="00E256FB">
																																																			<w:rPr>
																																																				<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																					<wx:font wx:val="仿宋"/>
																																																					<w:b/>
																																																					<w:sz w:val="30"/>
																																																					<w:sz-cs w:val="30"/>
																																																				</w:rPr>
																																																				<w:t>项目建设进展情况</w:t>
																																																			</w:r>
																																																		</w:p>
																																																		<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																			<w:pPr>
																																																				<w:spacing w:line="560" w:line-rule="exact"/>
																																																				<w:ind w:first-line-chars="200" w:first-line="723"/>
																																																				<w:rPr>
																																																					<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																					<wx:font wx:val="宋体"/>
																																																					<w:b/>
																																																					<w:sz w:val="36"/>
																																																					<w:sz-cs w:val="36"/>
																																																				</w:rPr>
																																																			</w:pPr>
																																																			<w:r wsp:rsidRPr="00E256FB">
																																																				<w:rPr>
																																																					<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																					<wx:font wx:val="宋体"/>
																																																					<w:b/>
																																																					<w:sz w:val="36"/>
																																																					<w:sz-cs w:val="36"/>
																																																				</w:rPr>
																																																				<w:t>${currentBusiness}</w:t>
																																																			</w:r>
																																																		</w:p>
																																																		<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																			<w:pPr>
																																																				<w:listPr>
																																																					<w:ilvl w:val="0"/>
																																																					<w:ilfo w:val="1"/>
																																																					<wx:t wx:val="四、"/>
																																																					<wx:font wx:val="Times New Roman"/>
																																																				</w:listPr>
																																																				<w:spacing w:line="560" w:line-rule="exact"/>
																																																				<w:rPr>
																																																					<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																						<wx:font wx:val="仿宋"/>
																																																						<w:b/>
																																																						<w:sz w:val="30"/>
																																																						<w:sz-cs w:val="30"/>
																																																					</w:rPr>
																																																				</w:pPr>
																																																				<w:r wsp:rsidRPr="00E256FB">
																																																					<w:rPr>
																																																						<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																							<wx:font wx:val="仿宋"/>
																																																							<w:b/>
																																																							<w:sz w:val="30"/>
																																																							<w:sz-cs w:val="30"/>
																																																						</w:rPr>
																																																						<w:t>团队建设开展情况</w:t>
																																																					</w:r>
																																																				</w:p>
																																																				<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																					<w:pPr>
																																																						<w:spacing w:line="560" w:line-rule="exact"/>
																																																						<w:ind w:left="420" w:first-line-chars="80" w:first-line="289"/>
																																																						<w:rPr>
																																																							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																							<wx:font wx:val="宋体"/>
																																																							<w:b/>
																																																							<w:sz w:val="36"/>
																																																							<w:sz-cs w:val="36"/>
																																																						</w:rPr>
																																																					</w:pPr>
																																																					<w:r wsp:rsidRPr="00E256FB">
																																																						<w:rPr>
																																																							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																								<wx:font wx:val="宋体"/>
																																																								<w:b/>
																																																								<w:sz w:val="36"/>
																																																								<w:sz-cs w:val="36"/>
																																																							</w:rPr>
																																																							<w:t>${currentTeamProcess}</w:t>
																																																						</w:r>
																																																					</w:p>
																																																					<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																						<w:pPr>
																																																							<w:listPr>
																																																								<w:ilvl w:val="0"/>
																																																								<w:ilfo w:val="1"/>
																																																								<wx:t wx:val="五、"/>
																																																								<wx:font wx:val="Times New Roman"/>
																																																							</w:listPr>
																																																							<w:spacing w:line="560" w:line-rule="exact"/>
																																																							<w:rPr>
																																																								<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																									<wx:font wx:val="仿宋"/>
																																																									<w:b/>
																																																									<w:sz w:val="30"/>
																																																									<w:sz-cs w:val="30"/>
																																																								</w:rPr>
																																																							</w:pPr>
																																																							<w:r wsp:rsidRPr="00E256FB">
																																																								<w:rPr>
																																																									<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																										<wx:font wx:val="仿宋"/>
																																																										<w:b/>
																																																										<w:sz w:val="30"/>
																																																										<w:sz-cs w:val="30"/>
																																																									</w:rPr>
																																																									<w:t>日常工作</w:t>
																																																								</w:r>
																																																							</w:p>
																																																							<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																								<w:pPr>
																																																									<w:spacing w:line="560" w:line-rule="exact"/>
																																																									<w:ind w:left="420" w:first-line-chars="80" w:first-line="289"/>
																																																									<w:rPr>
																																																										<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																											<wx:font wx:val="仿宋"/>
																																																											<w:b/>
																																																											<w:sz w:val="30"/>
																																																											<w:sz-cs w:val="30"/>
																																																										</w:rPr>
																																																									</w:pPr>
																																																									<w:r wsp:rsidRPr="00E256FB">
																																																										<w:rPr>
																																																											<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																												<wx:font wx:val="宋体"/>
																																																												<w:b/>
																																																												<w:sz w:val="36"/>
																																																												<w:sz-cs w:val="36"/>
																																																											</w:rPr>
																																																											<w:t>${currentDaily}</w:t>
																																																										</w:r>
																																																									</w:p>
																																																									<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																										<w:pPr>
																																																											<w:listPr>
																																																												<w:ilvl w:val="0"/>
																																																												<w:ilfo w:val="1"/>
																																																												<wx:t wx:val="六、"/>
																																																												<wx:font wx:val="Times New Roman"/>
																																																											</w:listPr>
																																																											<w:spacing w:line="560" w:line-rule="exact"/>
																																																											<w:rPr>
																																																												<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																													<wx:font wx:val="仿宋"/>
																																																													<w:b/>
																																																													<w:sz w:val="30"/>
																																																													<w:sz-cs w:val="30"/>
																																																												</w:rPr>
																																																											</w:pPr>
																																																											<w:r wsp:rsidRPr="00E256FB">
																																																												<w:rPr>
																																																													<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																														<wx:font wx:val="仿宋"/>
																																																														<w:b/>
																																																														<w:sz w:val="30"/>
																																																														<w:sz-cs w:val="30"/>
																																																													</w:rPr>
																																																													<w:t>存在问题及进展情况</w:t>
																																																												</w:r>
																																																											</w:p>
																																																											<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																												<w:pPr>
																																																													<w:spacing w:line="560" w:line-rule="exact"/>
																																																													<w:ind w:left="420" w:first-line-chars="80" w:first-line="289"/>
																																																													<w:rPr>
																																																														<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																														<wx:font wx:val="宋体"/>
																																																														<w:b/>
																																																														<w:sz w:val="36"/>
																																																														<w:sz-cs w:val="36"/>
																																																													</w:rPr>
																																																												</w:pPr>
																																																												<w:r wsp:rsidRPr="00E256FB">
																																																													<w:rPr>
																																																														<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																															<wx:font wx:val="宋体"/>
																																																															<w:b/>
																																																															<w:sz w:val="36"/>
																																																															<w:sz-cs w:val="36"/>
																																																														</w:rPr>
																																																														<w:t>${currentProblem}</w:t>
																																																													</w:r>
																																																												</w:p>
																																																												<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																													<w:pPr>
																																																														<w:listPr>
																																																															<w:ilvl w:val="0"/>
																																																															<w:ilfo w:val="1"/>
																																																															<wx:t wx:val="七、"/>
																																																															<wx:font wx:val="Times New Roman"/>
																																																														</w:listPr>
																																																														<w:spacing w:line="560" w:line-rule="exact"/>
																																																														<w:rPr>
																																																															<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																<wx:font wx:val="仿宋"/>
																																																																<w:b/>
																																																																<w:sz w:val="30"/>
																																																																<w:sz-cs w:val="30"/>
																																																															</w:rPr>
																																																														</w:pPr>
																																																														<w:r wsp:rsidRPr="00E256FB">
																																																															<w:rPr>
																																																																<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																	<wx:font wx:val="仿宋"/>
																																																																	<w:b/>
																																																																	<w:sz w:val="30"/>
																																																																	<w:sz-cs w:val="30"/>
																																																																</w:rPr>
																																																																<w:t>其他工作</w:t>
																																																															</w:r>
																																																														</w:p>
																																																														<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																															<w:pPr>
																																																																<w:spacing w:line="560" w:line-rule="exact"/>
																																																																<w:ind w:left="420" w:first-line-chars="80" w:first-line="289"/>
																																																																<w:rPr>
																																																																	<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																		<wx:font wx:val="仿宋"/>
																																																																		<w:b/>
																																																																		<w:sz w:val="30"/>
																																																																		<w:sz-cs w:val="30"/>
																																																																	</w:rPr>
																																																																</w:pPr>
																																																																<w:r wsp:rsidRPr="00E256FB">
																																																																	<w:rPr>
																																																																		<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																			<wx:font wx:val="宋体"/>
																																																																			<w:b/>
																																																																			<w:sz w:val="36"/>
																																																																			<w:sz-cs w:val="36"/>
																																																																		</w:rPr>
																																																																		<w:t>${currentOther}</w:t>
																																																																	</w:r>
																																																																</w:p>
																																																																<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																	<w:pPr>
																																																																		<w:spacing w:line="560" w:line-rule="exact"/>
																																																																		<w:rPr>
																																																																			<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体"/>
																																																																				<wx:font wx:val="黑体"/>
																																																																				<w:sz w:val="30"/>
																																																																			</w:rPr>
																																																																		</w:pPr>
																																																																		<w:r wsp:rsidRPr="00E256FB">
																																																																			<w:rPr>
																																																																				<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="黑体" w:hint="fareast"/>
																																																																					<wx:font wx:val="黑体"/>
																																																																					<w:sz w:val="30"/>
																																																																				</w:rPr>
																																																																				<w:t>下周工作计划：</w:t>
																																																																			</w:r>
																																																																		</w:p>
																																																																		<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																			<w:pPr>
																																																																				<w:listPr>
																																																																					<w:ilvl w:val="0"/>
																																																																					<w:ilfo w:val="4"/>
																																																																					<wx:t wx:val="一、"/>
																																																																					<wx:font wx:val="Times New Roman"/>
																																																																				</w:listPr>
																																																																				<w:spacing w:line="560" w:line-rule="exact"/>
																																																																				<w:rPr>
																																																																					<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																						<wx:font wx:val="仿宋"/>
																																																																						<w:b/>
																																																																						<w:sz w:val="30"/>
																																																																						<w:sz-cs w:val="30"/>
																																																																					</w:rPr>
																																																																				</w:pPr>
																																																																				<w:r wsp:rsidRPr="00E256FB">
																																																																					<w:rPr>
																																																																						<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																							<wx:font wx:val="仿宋"/>
																																																																							<w:b/>
																																																																							<w:sz w:val="30"/>
																																																																							<w:sz-cs w:val="30"/>
																																																																						</w:rPr>
																																																																						<w:t>重点工作进展情况</w:t>
																																																																					</w:r>
																																																																				</w:p>
																																																																				<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																					<w:pPr>
																																																																						<w:listPr>
																																																																							<w:ilvl w:val="0"/>
																																																																							<w:ilfo w:val="5"/>
																																																																							<wx:t wx:val="（一）"/>
																																																																							<wx:font wx:val="Times New Roman"/>
																																																																						</w:listPr>
																																																																						<w:spacing w:line="560" w:line-rule="exact"/>
																																																																						<w:rPr>
																																																																							<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																								<wx:font wx:val="仿宋"/>
																																																																								<w:b/>
																																																																								<w:sz w:val="30"/>
																																																																								<w:sz-cs w:val="30"/>
																																																																							</w:rPr>
																																																																						</w:pPr>
																																																																						<w:r wsp:rsidRPr="00E256FB">
																																																																							<w:rPr>
																																																																								<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																									<wx:font wx:val="仿宋"/>
																																																																									<w:b/>
																																																																									<w:sz w:val="30"/>
																																																																									<w:sz-cs w:val="30"/>
																																																																								</w:rPr>
																																																																								<w:t>新核心项目群</w:t>
																																																																							</w:r>
																																																																						</w:p>
																																																																						<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																							<w:pPr>
																																																																								<w:spacing w:line="560" w:line-rule="exact"/>
																																																																								<w:ind w:first-line-chars="196" w:first-line="708"/>
																																																																								<w:rPr>
																																																																									<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																										<wx:font wx:val="仿宋"/>
																																																																										<w:b/>
																																																																										<w:sz w:val="30"/>
																																																																										<w:sz-cs w:val="30"/>
																																																																									</w:rPr>
																																																																								</w:pPr>
																																																																								<w:r wsp:rsidRPr="00E256FB">
																																																																									<w:rPr>
																																																																										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																											<wx:font wx:val="宋体"/>
																																																																											<w:b/>
																																																																											<w:sz w:val="36"/>
																																																																											<w:sz-cs w:val="36"/>
																																																																										</w:rPr>
																																																																										<w:t>${nextNewCorecontent}</w:t>
																																																																									</w:r>
																																																																								</w:p>
																																																																								<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																									<w:pPr>
																																																																										<w:listPr>
																																																																											<w:ilvl w:val="0"/>
																																																																											<w:ilfo w:val="5"/>
																																																																											<wx:t wx:val="（二）"/>
																																																																											<wx:font wx:val="Times New Roman"/>
																																																																										</w:listPr>
																																																																										<w:spacing w:line="560" w:line-rule="exact"/>
																																																																										<w:rPr>
																																																																											<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																												<wx:font wx:val="仿宋"/>
																																																																												<w:b/>
																																																																												<w:sz w:val="30"/>
																																																																												<w:sz-cs w:val="30"/>
																																																																											</w:rPr>
																																																																										</w:pPr>
																																																																										<w:r wsp:rsidRPr="00E256FB">
																																																																											<w:rPr>
																																																																												<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																													<wx:font wx:val="仿宋"/>
																																																																													<w:b/>
																																																																													<w:sz w:val="30"/>
																																																																													<w:sz-cs w:val="30"/>
																																																																												</w:rPr>
																																																																												<w:t>两地三中心</w:t>
																																																																											</w:r>
																																																																										</w:p>
																																																																										<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																											<w:pPr>
																																																																												<w:spacing w:line="560" w:line-rule="exact"/>
																																																																												<w:ind w:first-line-chars="196" w:first-line="708"/>
																																																																												<w:rPr>
																																																																													<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																														<wx:font wx:val="仿宋"/>
																																																																														<w:b/>
																																																																														<w:sz w:val="30"/>
																																																																														<w:sz-cs w:val="30"/>
																																																																													</w:rPr>
																																																																												</w:pPr>
																																																																												<w:r wsp:rsidRPr="00E256FB">
																																																																													<w:rPr>
																																																																														<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																															<wx:font wx:val="宋体"/>
																																																																															<w:b/>
																																																																															<w:sz w:val="36"/>
																																																																															<w:sz-cs w:val="36"/>
																																																																														</w:rPr>
																																																																														<w:t>${nextTwoSecond}</w:t>
																																																																													</w:r>
																																																																												</w:p>
																																																																												<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																													<w:pPr>
																																																																														<w:listPr>
																																																																															<w:ilvl w:val="0"/>
																																																																															<w:ilfo w:val="4"/>
																																																																															<wx:t wx:val="二、"/>
																																																																															<wx:font wx:val="Times New Roman"/>
																																																																														</w:listPr>
																																																																														<w:spacing w:line="560" w:line-rule="exact"/>
																																																																														<w:rPr>
																																																																															<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																<wx:font wx:val="仿宋"/>
																																																																																<w:b/>
																																																																																<w:sz w:val="30"/>
																																																																																<w:sz-cs w:val="30"/>
																																																																															</w:rPr>
																																																																														</w:pPr>
																																																																														<w:r wsp:rsidRPr="00E256FB">
																																																																															<w:rPr>
																																																																																<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																	<wx:font wx:val="仿宋"/>
																																																																																	<w:b/>
																																																																																	<w:sz w:val="30"/>
																																																																																	<w:sz-cs w:val="30"/>
																																																																																</w:rPr>
																																																																																<w:t>基础管理工作</w:t>
																																																																															</w:r>
																																																																														</w:p>
																																																																														<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																															<w:pPr>
																																																																																<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																<w:ind w:first-line-chars="196" w:first-line="708"/>
																																																																																<w:rPr>
																																																																																	<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																	<wx:font wx:val="宋体"/>
																																																																																	<w:b/>
																																																																																	<w:sz w:val="36"/>
																																																																																	<w:sz-cs w:val="36"/>
																																																																																</w:rPr>
																																																																															</w:pPr>
																																																																															<w:r wsp:rsidRPr="00E256FB">
																																																																																<w:rPr>
																																																																																	<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																		<wx:font wx:val="宋体"/>
																																																																																		<w:b/>
																																																																																		<w:sz w:val="36"/>
																																																																																		<w:sz-cs w:val="36"/>
																																																																																	</w:rPr>
																																																																																	<w:t>${nextBasicManage}</w:t>
																																																																																</w:r>
																																																																															</w:p>
																																																																															<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																<w:pPr>
																																																																																	<w:listPr>
																																																																																		<w:ilvl w:val="0"/>
																																																																																		<w:ilfo w:val="4"/>
																																																																																		<wx:t wx:val="三、"/>
																																																																																		<wx:font wx:val="Times New Roman"/>
																																																																																	</w:listPr>
																																																																																	<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																	<w:rPr>
																																																																																		<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																			<wx:font wx:val="仿宋"/>
																																																																																			<w:b/>
																																																																																			<w:sz w:val="30"/>
																																																																																			<w:sz-cs w:val="30"/>
																																																																																		</w:rPr>
																																																																																	</w:pPr>
																																																																																	<w:r wsp:rsidRPr="00E256FB">
																																																																																		<w:rPr>
																																																																																			<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																				<wx:font wx:val="仿宋"/>
																																																																																				<w:b/>
																																																																																				<w:sz w:val="30"/>
																																																																																				<w:sz-cs w:val="30"/>
																																																																																			</w:rPr>
																																																																																			<w:t>项目建设情况</w:t>
																																																																																		</w:r>
																																																																																	</w:p>
																																																																																	<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																		<w:pPr>
																																																																																			<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																			<w:rPr>
																																																																																				<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																					<wx:font wx:val="仿宋"/>
																																																																																					<w:b/>
																																																																																					<w:sz w:val="30"/>
																																																																																					<w:sz-cs w:val="30"/>
																																																																																				</w:rPr>
																																																																																			</w:pPr>
																																																																																			<w:r wsp:rsidRPr="00E256FB">
																																																																																				<w:rPr>
																																																																																					<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																						<wx:font wx:val="仿宋"/>
																																																																																						<w:b/>
																																																																																						<w:sz w:val="30"/>
																																																																																						<w:sz-cs w:val="30"/>
																																																																																					</w:rPr>
																																																																																					<w:t>（一）上线投产情况</w:t>
																																																																																				</w:r>
																																																																																			</w:p>
																																																																																			<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																				<w:pPr>
																																																																																					<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																					<w:ind w:left-chars="200" w:left="420"/>
																																																																																					<w:rPr>
																																																																																						<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																							<wx:font wx:val="仿宋"/>
																																																																																							<w:sz w:val="30"/>
																																																																																							<w:sz-cs w:val="30"/>
																																																																																						</w:rPr>
																																																																																					</w:pPr>
																																																																																					<w:r wsp:rsidRPr="00E256FB">
																																																																																						<w:rPr>
																																																																																							<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																								<wx:font wx:val="仿宋"/>
																																																																																								<w:sz w:val="30"/>
																																																																																								<w:sz-cs w:val="30"/>
																																																																																							</w:rPr>
																																																																																							<w:t>1.项目方面</w:t>
																																																																																						</w:r>
																																																																																					</w:p>
																																																																																					<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																						<w:pPr>
																																																																																							<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																							<w:ind w:first-line-chars="216" w:first-line="781"/>
																																																																																							<w:rPr>
																																																																																								<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																								<wx:font wx:val="宋体"/>
																																																																																								<w:b/>
																																																																																								<w:sz w:val="36"/>
																																																																																								<w:sz-cs w:val="36"/>
																																																																																							</w:rPr>
																																																																																						</w:pPr>
																																																																																						<w:r wsp:rsidRPr="00E256FB">
																																																																																							<w:rPr>
																																																																																								<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																									<wx:font wx:val="宋体"/>
																																																																																									<w:b/>
																																																																																									<w:sz w:val="36"/>
																																																																																									<w:sz-cs w:val="36"/>
																																																																																								</w:rPr>
																																																																																								<w:t>${nextProjectUpdate}</w:t>
																																																																																							</w:r>
																																																																																						</w:p>
																																																																																						<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																							<w:pPr>
																																																																																								<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																								<w:ind w:left-chars="200" w:left="420"/>
																																																																																								<w:rPr>
																																																																																									<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																										<wx:font wx:val="仿宋"/>
																																																																																										<w:sz w:val="30"/>
																																																																																										<w:sz-cs w:val="30"/>
																																																																																									</w:rPr>
																																																																																								</w:pPr>
																																																																																								<w:r wsp:rsidRPr="00E256FB">
																																																																																									<w:rPr>
																																																																																										<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																											<wx:font wx:val="仿宋"/>
																																																																																											<w:sz w:val="30"/>
																																																																																											<w:sz-cs w:val="30"/>
																																																																																										</w:rPr>
																																																																																										<w:t>2.需求方面</w:t>
																																																																																									</w:r>
																																																																																								</w:p>
																																																																																								<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																									<w:pPr>
																																																																																										<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																										<w:ind w:first-line-chars="216" w:first-line="781"/>
																																																																																										<w:rPr>
																																																																																											<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																											<wx:font wx:val="宋体"/>
																																																																																											<w:b/>
																																																																																											<w:sz w:val="36"/>
																																																																																											<w:sz-cs w:val="36"/>
																																																																																										</w:rPr>
																																																																																									</w:pPr>
																																																																																									<w:r wsp:rsidRPr="00E256FB">
																																																																																										<w:rPr>
																																																																																											<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																												<wx:font wx:val="宋体"/>
																																																																																												<w:b/>
																																																																																												<w:sz w:val="36"/>
																																																																																												<w:sz-cs w:val="36"/>
																																																																																											</w:rPr>
																																																																																											<w:t>${nextDemand}</w:t>
																																																																																										</w:r>
																																																																																									</w:p>
																																																																																									<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																										<w:pPr>
																																																																																											<w:listPr>
																																																																																												<w:ilvl w:val="0"/>
																																																																																												<w:ilfo w:val="3"/>
																																																																																												<wx:t wx:val="（五）"/>
																																																																																												<wx:font wx:val="Times New Roman"/>
																																																																																											</w:listPr>
																																																																																											<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																											<w:rPr>
																																																																																												<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																													<wx:font wx:val="仿宋"/>
																																																																																													<w:b/>
																																																																																													<w:sz w:val="30"/>
																																																																																													<w:sz-cs w:val="30"/>
																																																																																												</w:rPr>
																																																																																											</w:pPr>
																																																																																											<w:r wsp:rsidRPr="00E256FB">
																																																																																												<w:rPr>
																																																																																													<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																														<wx:font wx:val="仿宋"/>
																																																																																														<w:b/>
																																																																																														<w:sz w:val="30"/>
																																																																																														<w:sz-cs w:val="30"/>
																																																																																													</w:rPr>
																																																																																													<w:t>招标进展情况</w:t>
																																																																																												</w:r>
																																																																																											</w:p>
																																																																																											<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																												<w:pPr>
																																																																																													<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																													<w:ind w:left="420" w:first-line-chars="100" w:first-line="361"/>
																																																																																													<w:rPr>
																																																																																														<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																														<wx:font wx:val="宋体"/>
																																																																																														<w:b/>
																																																																																														<w:sz w:val="36"/>
																																																																																														<w:sz-cs w:val="36"/>
																																																																																													</w:rPr>
																																																																																												</w:pPr>
																																																																																												<w:r wsp:rsidRPr="00E256FB">
																																																																																													<w:rPr>
																																																																																														<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																															<wx:font wx:val="宋体"/>
																																																																																															<w:b/>
																																																																																															<w:sz w:val="36"/>
																																																																																															<w:sz-cs w:val="36"/>
																																																																																														</w:rPr>
																																																																																														<w:t>${nextBid}</w:t>
																																																																																													</w:r>
																																																																																												</w:p>
																																																																																												<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																													<w:pPr>
																																																																																														<w:listPr>
																																																																																															<w:ilvl w:val="0"/>
																																																																																															<w:ilfo w:val="3"/>
																																																																																															<wx:t wx:val="（六）"/>
																																																																																															<wx:font wx:val="Times New Roman"/>
																																																																																														</w:listPr>
																																																																																														<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																														<w:rPr>
																																																																																															<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																<wx:font wx:val="仿宋"/>
																																																																																																<w:b/>
																																																																																																<w:sz w:val="30"/>
																																																																																																<w:sz-cs w:val="30"/>
																																																																																															</w:rPr>
																																																																																														</w:pPr>
																																																																																														<w:r wsp:rsidRPr="00E256FB">
																																																																																															<w:rPr>
																																																																																																<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																																	<wx:font wx:val="仿宋"/>
																																																																																																	<w:b/>
																																																																																																	<w:sz w:val="30"/>
																																																																																																	<w:sz-cs w:val="30"/>
																																																																																																</w:rPr>
																																																																																																<w:t>商务进展情况</w:t>
																																																																																															</w:r>
																																																																																														</w:p>
																																																																																														<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																															<w:pPr>
																																																																																																<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																<w:ind w:first-line-chars="200" w:first-line="723"/>
																																																																																																<w:rPr>
																																																																																																	<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																																	<wx:font wx:val="宋体"/>
																																																																																																	<w:b/>
																																																																																																	<w:sz w:val="36"/>
																																																																																																	<w:sz-cs w:val="36"/>
																																																																																																</w:rPr>
																																																																																															</w:pPr>
																																																																																															<w:r wsp:rsidRPr="00E256FB">
																																																																																																<w:rPr>
																																																																																																	<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																																		<wx:font wx:val="宋体"/>
																																																																																																		<w:b/>
																																																																																																		<w:sz w:val="36"/>
																																																																																																		<w:sz-cs w:val="36"/>
																																																																																																	</w:rPr>
																																																																																																	<w:t>${nextBusiness}</w:t>
																																																																																																</w:r>
																																																																																															</w:p>
																																																																																															<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																<w:pPr>
																																																																																																	<w:listPr>
																																																																																																		<w:ilvl w:val="0"/>
																																																																																																		<w:ilfo w:val="3"/>
																																																																																																		<wx:t wx:val="（七）"/>
																																																																																																		<wx:font wx:val="Times New Roman"/>
																																																																																																	</w:listPr>
																																																																																																	<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																	<w:rPr>
																																																																																																		<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																			<wx:font wx:val="仿宋"/>
																																																																																																			<w:b/>
																																																																																																			<w:sz w:val="30"/>
																																																																																																			<w:sz-cs w:val="30"/>
																																																																																																		</w:rPr>
																																																																																																	</w:pPr>
																																																																																																	<w:r wsp:rsidRPr="00E256FB">
																																																																																																		<w:rPr>
																																																																																																			<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																																				<wx:font wx:val="仿宋"/>
																																																																																																				<w:b/>
																																																																																																				<w:sz w:val="30"/>
																																																																																																				<w:sz-cs w:val="30"/>
																																																																																																			</w:rPr>
																																																																																																			<w:t>项目建设进展情况</w:t>
																																																																																																		</w:r>
																																																																																																	</w:p>
																																																																																																	<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																		<w:pPr>
																																																																																																			<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																			<w:ind w:left="420" w:first-line-chars="100" w:first-line="361"/>
																																																																																																			<w:rPr>
																																																																																																				<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																																				<wx:font wx:val="宋体"/>
																																																																																																				<w:b/>
																																																																																																				<w:sz w:val="36"/>
																																																																																																				<w:sz-cs w:val="36"/>
																																																																																																			</w:rPr>
																																																																																																		</w:pPr>
																																																																																																		<w:r wsp:rsidRPr="00E256FB">
																																																																																																			<w:rPr>
																																																																																																				<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																																					<wx:font wx:val="宋体"/>
																																																																																																					<w:b/>
																																																																																																					<w:sz w:val="36"/>
																																																																																																					<w:sz-cs w:val="36"/>
																																																																																																				</w:rPr>
																																																																																																				<w:t>${nextProjectProgess}</w:t>
																																																																																																			</w:r>
																																																																																																		</w:p>
																																																																																																		<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																			<w:pPr>
																																																																																																				<w:listPr>
																																																																																																					<w:ilvl w:val="0"/>
																																																																																																					<w:ilfo w:val="4"/>
																																																																																																					<wx:t wx:val="四、"/>
																																																																																																					<wx:font wx:val="Times New Roman"/>
																																																																																																				</w:listPr>
																																																																																																				<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																				<w:rPr>
																																																																																																					<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																						<wx:font wx:val="仿宋"/>
																																																																																																						<w:b/>
																																																																																																						<w:sz w:val="30"/>
																																																																																																						<w:sz-cs w:val="30"/>
																																																																																																					</w:rPr>
																																																																																																				</w:pPr>
																																																																																																				<w:r wsp:rsidRPr="00E256FB">
																																																																																																					<w:rPr>
																																																																																																						<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																																							<wx:font wx:val="仿宋"/>
																																																																																																							<w:b/>
																																																																																																							<w:sz w:val="30"/>
																																																																																																							<w:sz-cs w:val="30"/>
																																																																																																						</w:rPr>
																																																																																																						<w:t>团队建设开展情况</w:t>
																																																																																																					</w:r>
																																																																																																				</w:p>
																																																																																																				<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																					<w:pPr>
																																																																																																						<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																						<w:ind w:left="420" w:first-line-chars="80" w:first-line="289"/>
																																																																																																						<w:rPr>
																																																																																																							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																																							<wx:font wx:val="宋体"/>
																																																																																																							<w:b/>
																																																																																																							<w:sz w:val="36"/>
																																																																																																							<w:sz-cs w:val="36"/>
																																																																																																						</w:rPr>
																																																																																																					</w:pPr>
																																																																																																					<w:r wsp:rsidRPr="00E256FB">
																																																																																																						<w:rPr>
																																																																																																							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																																								<wx:font wx:val="宋体"/>
																																																																																																								<w:b/>
																																																																																																								<w:sz w:val="36"/>
																																																																																																								<w:sz-cs w:val="36"/>
																																																																																																							</w:rPr>
																																																																																																							<w:t>${nextTeamProcess}</w:t>
																																																																																																						</w:r>
																																																																																																					</w:p>
																																																																																																					<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																						<w:pPr>
																																																																																																							<w:listPr>
																																																																																																								<w:ilvl w:val="0"/>
																																																																																																								<w:ilfo w:val="4"/>
																																																																																																								<wx:t wx:val="五、"/>
																																																																																																								<wx:font wx:val="Times New Roman"/>
																																																																																																							</w:listPr>
																																																																																																							<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																							<w:rPr>
																																																																																																								<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																									<wx:font wx:val="仿宋"/>
																																																																																																									<w:b/>
																																																																																																									<w:sz w:val="30"/>
																																																																																																									<w:sz-cs w:val="30"/>
																																																																																																								</w:rPr>
																																																																																																							</w:pPr>
																																																																																																							<w:r wsp:rsidRPr="00E256FB">
																																																																																																								<w:rPr>
																																																																																																									<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																																										<wx:font wx:val="仿宋"/>
																																																																																																										<w:b/>
																																																																																																										<w:sz w:val="30"/>
																																																																																																										<w:sz-cs w:val="30"/>
																																																																																																									</w:rPr>
																																																																																																									<w:t>日常工作</w:t>
																																																																																																								</w:r>
																																																																																																							</w:p>
																																																																																																							<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																								<w:pPr>
																																																																																																									<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																									<w:ind w:left="420" w:first-line-chars="80" w:first-line="289"/>
																																																																																																									<w:rPr>
																																																																																																										<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																																										<wx:font wx:val="宋体"/>
																																																																																																										<w:b/>
																																																																																																										<w:sz w:val="36"/>
																																																																																																										<w:sz-cs w:val="36"/>
																																																																																																									</w:rPr>
																																																																																																								</w:pPr>
																																																																																																								<w:r wsp:rsidRPr="00E256FB">
																																																																																																									<w:rPr>
																																																																																																										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																																											<wx:font wx:val="宋体"/>
																																																																																																											<w:b/>
																																																																																																											<w:sz w:val="36"/>
																																																																																																											<w:sz-cs w:val="36"/>
																																																																																																										</w:rPr>
																																																																																																										<w:t>${nextDaily}</w:t>
																																																																																																									</w:r>
																																																																																																								</w:p>
																																																																																																								<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																									<w:pPr>
																																																																																																										<w:listPr>
																																																																																																											<w:ilvl w:val="0"/>
																																																																																																											<w:ilfo w:val="4"/>
																																																																																																											<wx:t wx:val="六、"/>
																																																																																																											<wx:font wx:val="Times New Roman"/>
																																																																																																										</w:listPr>
																																																																																																										<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																										<w:rPr>
																																																																																																											<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																												<wx:font wx:val="仿宋"/>
																																																																																																												<w:b/>
																																																																																																												<w:sz w:val="30"/>
																																																																																																												<w:sz-cs w:val="30"/>
																																																																																																											</w:rPr>
																																																																																																										</w:pPr>
																																																																																																										<w:r wsp:rsidRPr="00E256FB">
																																																																																																											<w:rPr>
																																																																																																												<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																																													<wx:font wx:val="仿宋"/>
																																																																																																													<w:b/>
																																																																																																													<w:sz w:val="30"/>
																																																																																																													<w:sz-cs w:val="30"/>
																																																																																																												</w:rPr>
																																																																																																												<w:t>存在问题及进展情况</w:t>
																																																																																																											</w:r>
																																																																																																										</w:p>
																																																																																																										<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																											<w:pPr>
																																																																																																												<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																												<w:ind w:left="420" w:first-line-chars="100" w:first-line="361"/>
																																																																																																												<w:rPr>
																																																																																																													<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																																													<wx:font wx:val="宋体"/>
																																																																																																													<w:b/>
																																																																																																													<w:sz w:val="36"/>
																																																																																																													<w:sz-cs w:val="36"/>
																																																																																																												</w:rPr>
																																																																																																											</w:pPr>
																																																																																																											<w:r wsp:rsidRPr="00E256FB">
																																																																																																												<w:rPr>
																																																																																																													<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																																														<wx:font wx:val="宋体"/>
																																																																																																														<w:b/>
																																																																																																														<w:sz w:val="36"/>
																																																																																																														<w:sz-cs w:val="36"/>
																																																																																																													</w:rPr>
																																																																																																													<w:t>${nextProblem}</w:t>
																																																																																																												</w:r>
																																																																																																											</w:p>
																																																																																																											<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																												<w:pPr>
																																																																																																													<w:listPr>
																																																																																																														<w:ilvl w:val="0"/>
																																																																																																														<w:ilfo w:val="4"/>
																																																																																																														<wx:t wx:val="七、"/>
																																																																																																														<wx:font wx:val="Times New Roman"/>
																																																																																																													</w:listPr>
																																																																																																													<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																													<w:rPr>
																																																																																																														<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																															<wx:font wx:val="仿宋"/>
																																																																																																															<w:b/>
																																																																																																															<w:sz w:val="30"/>
																																																																																																															<w:sz-cs w:val="30"/>
																																																																																																														</w:rPr>
																																																																																																													</w:pPr>
																																																																																																													<w:r wsp:rsidRPr="00E256FB">
																																																																																																														<w:rPr>
																																																																																																															<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋" w:hint="fareast"/>
																																																																																																																<wx:font wx:val="仿宋"/>
																																																																																																																<w:b/>
																																																																																																																<w:sz w:val="30"/>
																																																																																																																<w:sz-cs w:val="30"/>
																																																																																																															</w:rPr>
																																																																																																															<w:t>其他工作</w:t>
																																																																																																														</w:r>
																																																																																																													</w:p>
																																																																																																													<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																														<w:pPr>
																																																																																																															<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																															<w:ind w:first-line-chars="196" w:first-line="708"/>
																																																																																																															<w:rPr>
																																																																																																																<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
																																																																																																																<wx:font wx:val="宋体"/>
																																																																																																																<w:b/>
																																																																																																																<w:sz w:val="36"/>
																																																																																																																<w:sz-cs w:val="36"/>
																																																																																																															</w:rPr>
																																																																																																														</w:pPr>
																																																																																																														<w:r wsp:rsidRPr="00E256FB">
																																																																																																															<w:rPr>
																																																																																																																<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
																																																																																																																	<wx:font wx:val="宋体"/>
																																																																																																																	<w:b/>
																																																																																																																	<w:sz w:val="36"/>
																																																																																																																	<w:sz-cs w:val="36"/>
																																																																																																																</w:rPr>
																																																																																																																<w:t>${nextOther}</w:t>
																																																																																																															</w:r>
																																																																																																														</w:p>
																																																																																																														<w:p wsp:rsidR="00E256FB" wsp:rsidRPr="00E256FB" wsp:rsidRDefault="00E256FB" wsp:rsidP="00E256FB">
																																																																																																															<w:pPr>
																																																																																																																<w:spacing w:line="560" w:line-rule="exact"/>
																																																																																																																<w:rPr>
																																																																																																																	<w:rFonts w:ascii="仿宋" w:fareast="仿宋" w:h-ansi="仿宋"/>
																																																																																																																		<wx:font wx:val="仿宋"/>
																																																																																																																		<w:b/>
																																																																																																																		<w:sz w:val="30"/>
																																																																																																																		<w:sz-cs w:val="30"/>
																																																																																																																	</w:rPr>
																																																																																																																</w:pPr>
																																																																																																															</w:p>
																																																																																																															<w:p wsp:rsidR="000A474C" wsp:rsidRPr="009B14BD" wsp:rsidRDefault="000A474C"/>
																																																																																																															<w:sectPr wsp:rsidR="000A474C" wsp:rsidRPr="009B14BD" wsp:rsidSect="008E760D">
																																																																																																																<w:pgSz w:w="11906" w:h="16838"/>
																																																																																																																<w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
																																																																																																																<w:cols w:space="425"/>
																																																																																																																<w:docGrid w:type="lines" w:line-pitch="312"/>
																																																																																																															</w:sectPr>
																																																																																																														</wx:sect>
																																																																																																													</w:body>
																																																																																																												</w:wordDocument>
