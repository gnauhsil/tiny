1.模板编写：
    word 中编写模板,另存为 xml 格式文件, 编辑 xml 格式,去除无用修饰符
    ${xxx}中的xxx格式与其前方的文字不一致, 生成xml有一些修饰xxx样式的标签,
    如字体,颜色等,word中看似写的是${xxx}实际上转为xml后变成
    ${<w:color ...>xxx</w:color>},
    该标签 FreeMarker 无法解析
  办法: 去掉${}内部的标签只留下xxx或者删掉 "${" 和 "}"